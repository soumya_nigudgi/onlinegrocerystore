Rails.application.routes.draw do 
  devise_for :staff_members, path: 'staff_members', controllers: {
    registrations: 'staff_members/custom_devise/registrations',
    sessions: 'staff_members/custom_devise/sessions'
  }
  
  namespace :staff_members do
    root 'products#index'

    resources :products do
      resources :prices
      resources :stocks
    end
  end

  devise_for :customers, :controllers => {
    :registrations => "custom_devise/registrations",
    :sessions => "custom_devise/sessions",
  }
    root 'products#main'

    # Product
    resources :products, only: :show

    # Search
    get '/search/products', to: 'search#products'
    
    # Carts
    put '/carts/add_product', to: 'carts#add_product'
    put '/carts/update_product', to: 'carts#update_product'
    delete '/carts/delete_product', to: 'carts#delete_product'
    get '/carts/products', to: 'carts#products'
    put  '/carts', to: 'carts#update'

    # Orders
    post  '/orders', to: 'orders#create'
    get  '/orders', to: 'orders#index'
    
    # Checkout
    get '/checkout', to: 'checkouts#view'
    put '/checkout/review', to: 'checkouts#review'

    # Credit Card
    get '/credit_cards/new', to: 'credit_cards#new'
    get '/credit_cards', to: 'credit_cards#index'
    post '/credit_cards', to: 'credit_cards#create'
    resources :credit_cards, only: [:edit, :update, :destroy]

    # Address
    get '/addresses/new', to: 'addresses#new'
    get '/addresses', to: 'addresses#index'
    post '/addresses', to: 'addresses#create'
    resources :addresses, only: [:edit, :update, :destroy]

end
