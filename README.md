# Online Grocery Store

  Demo Video: [docs/project_demo.mov](docs/project_demo.mov)
  
## Ruby and Rails versions

```text
Ruby: 2.6.3p62
Rails: 6.0.3
```

## System dependencies

* [PostgreSQL](https://www.postgresql.org/) - v13.0
* [Ruby](https://www.ruby-lang.org/en/) - v2.6.3p62
* [Bundler](https://bundler.io/) - v2.1.4
* [Node.js](https://nodejs.org/en/) - v14.15.0
* [Yarn](https://yarnpkg.com/) - v1.22.10

## Configuration

* Install PostgresSQL, Ruby, node, bundler, and yarn
* Move to the app directory: `cd [download-path]/onlinegrocerystore`
* Run: `bundle install`
* Update `config/database.yml` with username, password, and database

## Data files

* ER Diagram: [docs/ERDiagram.png](docs/ERDiagram.png)
* Database schema: [db/grocery_store.sql](db/grocery_store.sql)
* Sample data: [db/grocery_store_data.sql](db/grocery_store_data.sql)
* Database creation
* Create with rails: `rails db:create`
* Create with psql: `CREATE DATABASE grocery_store`
* Connect to the database: `\c grocery_store`
* Create tables `\i [download-path]/onlinegrocerystore/db/grocery_store.sql`
* Import data: `\i [download-path]/onlinegrocerystore/db/grocery_store_data.sql`

## How to run the application

```bash
    cd [download-path]/onlinegrocerystore
    rails s
```

## Application URL

<http://localhost:3000>
