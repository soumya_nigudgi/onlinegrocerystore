class CheckoutsController < ApplicationController
    before_action :authenticate_customer!, :fetch_customer_cart
    layout "checkout"

    def view
        @credit_cards = current_customer.credit_cards
        @shipping_addresses = current_customer.addresses
    end

    def review
        credit_card_id = params[:credit_card_id]
        address_id = params[:address_id]
        credit_card = current_customer.credit_cards.find(credit_card_id)
        address = current_customer.addresses.find(address_id)

        if address.state !=  @cart.cart_shipping_state
            alert_message = 'Selected shipping state is different from the cart. '
            alert_message += 'Please update the state of the cart or select a different shipping address'
            flash[:alert] = alert_message
            redirect_to checkout_path and return
        end

        @cart.update!(credit_card: credit_card, address: address)
    end

    private

    def fetch_customer_cart
        @cart = current_customer.cart
    end
end
