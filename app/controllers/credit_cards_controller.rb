class CreditCardsController < ApplicationController
    before_action :authenticate_customer!
    layout 'checkout'

    def index
        @credit_cards = current_customer.credit_cards
    end

    def edit
        @credit_card = current_customer.credit_cards.find(params[:id])
        @credit_card.address.split_name!
    end

    def update
        @credit_card = current_customer.credit_cards.find(params[:id])
        @credit_card.update(create_params)
        if @credit_card.errors.present?
            flash[:alert] = @credit_card.errors.messages
            render 'credit_cards/edit' and return
        end
        redirect_to credit_cards_path
    rescue StandardError => e
        flash[:alert] = e.message
        render 'credit_cards/edit'
    end

    def destroy
        @credit_card = current_customer.credit_cards.find(params[:id])
        @credit_card.destroy
        redirect_to credit_cards_path
    end

    def new
        @credit_card = CreditCard.new
        @credit_card.build_address
    end

    def create
        @credit_card = current_customer.credit_cards.build(create_params)
        @credit_card.save
        if @credit_card.errors.present?
            flash[:alert] = @credit_card.errors.messages
            render 'credit_cards/new' and return
        end
        redirect_to checkout_path
    rescue StandardError => e
        flash[:alert] = e.message
        render 'credit_cards/new'
    end

    private

    def create_params
        params.require(:credit_card).permit(:card_number, :name_on_card, :expiry_date,
             address_attributes: [
                 :first_name, :last_name, :address_line1,
                 :address_line2, :zipcode, :city, :state, :phone_number
                 ])
    end
end
