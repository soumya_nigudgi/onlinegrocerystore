class ProductsController < ApplicationController
    before_action :authenticate_customer!, except: [:main, :show]
    before_action :fetch_customer_cart, if: :customer_signed_in?
    layout 'product'

    def main
        @categories = Category.all
        @selected_category_id = params['category_id'] || @categories[0].id
        if @selected_category_id.present?
            category = Category.find(@selected_category_id)
            @products = category.products
        else
            @products = Product.all
        end
        @cart_shipping_state = @cart.present? ? @cart.cart_shipping_state : AppConstants::STATES[0]
    end

    def show
        @product = Product.find(params[:id])
        @search_disabled = true
    end

    private

    def fetch_customer_cart
        @cart = current_customer.cart
    end
end