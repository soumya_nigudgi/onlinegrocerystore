class CartsController < ApplicationController
    before_action :authenticate_customer!, :fetch_customer_cart
    layout 'checkout'

    def update
        cart_shipping_state = params[:cart_shipping_state]
        @cart.update!(cart_shipping_state: cart_shipping_state)
        redirect_to root_path(request.query_parameters)
    end

    def update_product
        product_id =  params['product_id']
        quantity =  params['quantity']
        order_product = @cart.order_products.where(product_id: product_id)[0]
        order_product.update!(quantity: quantity)
        redirect_to carts_products_path
    end

    def delete_product
        product_id = params['product_id']
        order_product = @cart.order_products.where(product_id: product_id)[0]
        order_product.destroy!
        redirect_to carts_products_path
    end

    def add_product
        product_id =  params['product_id']
        product = Product.find(product_id)
        @cart.order_products.create!(product: product, quantity: 1, status: :cart)
        parameters = params[:category_id].present? ? {category_id: params[:category_id]} : {}
        redirect_to root_path(parameters)
    end

    def products
        @order_products = @cart.order_products.order(:id)
    end

    private

    def fetch_customer_cart
        @cart = current_customer.cart
    end
end
