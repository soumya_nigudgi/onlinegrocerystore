class AddressesController < ApplicationController
    before_action :authenticate_customer!
    layout 'checkout'

    def index
        @addresses = current_customer.addresses
    end

    def new
        @address = Address.new
    end

    def create
        @address = current_customer.addresses.create(create_params)
        if @address.errors.present?
            flash[:alert] = @address.errors.messages
            render 'addresses/new' and return
        end
        redirect_to checkout_path
    rescue StandardError => e
        flash[:alert] = e.message
        render 'addresses/new'
    end

    def edit
        @address = current_customer.addresses.find(params[:id])
        @address.split_name!
        
    end

    def update
        @address = current_customer.addresses.find(params[:id])
        @address.update!(create_params)
        if @address.errors.present?
            flash[:alert] = @address.errors.messages
            render 'addresses/edit' and return
        end
        redirect_to addresses_path
    rescue StandardError => e
        flash[:alert] = e.message
        render 'addresses/edit'

    end

    def destroy
        @address = current_customer.addresses.find(params[:id])
        @address.destroy
        redirect_to addresses_path
    end

    private

    def create_params
        params.require(:address).permit(:first_name, :last_name, :address_line1,
                :address_line2, :zipcode, :city, :state, :phone_number)
    end
end
