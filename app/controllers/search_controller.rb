class SearchController < ApplicationController
    before_action :authenticate_customer!, :fetch_customer_cart
    layout 'product'
    
    def products
        @categories = Category.all
        query_string = params["q"]
        @products = Product.where("product_name ilike '%#{query_string}%'")
        @selected_category_id = params[:category_id] if params[:category_id].present?
        render 'products/main'
    end

    private

    def fetch_customer_cart
        @cart = current_customer.cart
    end
end
