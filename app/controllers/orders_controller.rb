class OrdersController < ApplicationController
    before_action :authenticate_customer!, :fetch_customer_cart
    layout 'checkout'

    def add_product
        product_id =  params['product_id']
        product = Product.find(product_id)
        @cart.order_products.create!(product: product, quantity: 1, status: :cart)
        redirect_to root_path
    end

    def create
        cart_created = @cart.create_order

        if cart_created.errors.blank?
            current_customer.create_cart!
        else
            flash[:alert] = cart_created.errors.messages
            render 'checkouts/review' and return
        end
    rescue StandardError => e
        flash[:alert] = e.message
        render 'checkouts/review' and return
    end

    def index
        @orders = current_customer.orders.where.not(status: :cart)
    end

    private

    def fetch_customer_cart
        @cart = current_customer.cart
    end
end
