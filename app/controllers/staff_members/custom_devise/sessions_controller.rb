# frozen_string_literal: true
module StaffMembers
    module CustomDevise
        class SessionsController < Devise::SessionsController
            include Accessible
            skip_before_action :check_user, only: :destroy
        end
    end
end
