# frozen_string_literal: true
module StaffMembers
  module CustomDevise
    class RegistrationsController < Devise::RegistrationsController
      include Accessible
      skip_before_action :check_user, only: :destroy
      
      private

      def sign_up_params
          params.require(:staff_member).permit(:email, :encrypted_password, :job_title, :job_type,
              :password, :password_confirmation,:first_name, :last_name, :contact_number,
              address_attributes: [
                :first_name, :last_name, :address_line1,
                :address_line2, :zipcode, :city, :state, :phone_number
              ])
      end
    end
  end
end
