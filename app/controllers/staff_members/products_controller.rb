class StaffMembers::ProductsController < ApplicationController
    before_action :authenticate_staff_member!
    layout 'staff_member'

    def index
        @products = Product.all.order('updated_at desc')
    end

    def new
        @product = Product.new
        all_categories = Category.all
        @catergory_options = category_name_and_ids(all_categories)
        @category_additional_attr = fetch_category_additional_attrs(all_categories)
    end

    def edit
        @product = Product.find(params[:id])
        all_categories = Category.all
        @catergory_options = category_name_and_ids(all_categories)
        @category_additional_attr = fetch_category_additional_attrs(all_categories)
    end

    def show
        @product = Product.find(params[:id])
    end

    def create
        all_categories = Category.all
        @catergory_options = category_name_and_ids(all_categories)
        @product = Product.new(create_params)
        @product.save
        if @product.errors.present?
            flash[:alert] = @product.errors.messages
            render 'staff_members/products/new' and return
        end
        redirect_to staff_members_products_path
    rescue StandardError => e
        flash[:alert] = e.message
        render 'staff_members/products/new'
    end

    def update
        all_categories = Category.all
        @catergory_options = category_name_and_ids(all_categories)
        @product = Product.find(params[:id])
        @product.update(create_params)
        if @product.errors.present?
            flash[:alert] = @product.errors.messages
            render 'staff_members/products/edit' and return
        end
        redirect_to staff_members_products_path
    rescue StandardError => e
        flash[:alert] = e.message
        render 'staff_members/products/edit'
    end

    def destroy
        @product = Product.find(params[:id])
        @product.destroy
        redirect_to staff_members_products_path
    end

    private


    def create_params
        params.require(:product).permit(:product_name, :product_size, :product_weight,
            :category_id, :product_description, :additional_info, :product_image)
    end

    def category_name_and_ids(all_categories)
        all_categories.collect {|c| [c.category_name, c.id]}
    end

    def fetch_category_additional_attrs(all_categories)
        category_additional_attrs = {}
        all_categories.each do|category|
            attr_hash = {}
            category.additional_info_attributes.map do|attr|
                attr_hash[attr.name] = ""
            end
            category_additional_attrs[category.id] = attr_hash
        end
        category_additional_attrs
    end
end
