module StaffMembers
    class PricesController < ApplicationController
        before_action :authenticate_staff_member!
        layout 'staff_member'

        def index
            product_id = params[:product_id]
            @product = Product.find(product_id)
            @prices = @product.prices
        end

        def new
            product_id = params[:product_id]
            @product = Product.find(product_id)
            @price = Price.new
        end

        def create
            product_id = params[:product_id]
            @product = Product.find(product_id)
            @price = @product.prices.build(create_params)
            @price.save
            if @price.errors.present?
                flash[:alert] = @price.errors.messages
                render 'staff_members/prices/new' and return
            end
            redirect_to staff_members_product_prices_path(@product.id)
        rescue StandardError => e
            flash[:alert] = e.message
            render 'staff_members/prices/new' and return
        end

        def edit
            product_id = params[:product_id]
            @product = Product.find(product_id)
            @price = @product.prices.find(params[:id])
        end


        def update
            product_id = params[:product_id]
            @product = Product.find(product_id)
            @price = @product.prices.find(params[:id])
            @price.update(create_params)
            if @price.errors.present?
                flash[:alert] = @price.errors.messages
                render 'staff_members/prices/new' and return
            end
            redirect_to staff_members_product_prices_path(@product.id)
        rescue StandardError => e
            flash[:alert] = e.message
            render 'staff_members/prices/new' and return
        end


        def destroy
            product_id = params[:product_id]
            @product = Product.find(product_id)
            @price = @product.prices.find(params[:id])
            @price.destroy
            redirect_to staff_members_product_prices_path(@product.id)
        end

        private

        def create_params
            params.require(:price).permit(:price, :state)
        end
    end
end