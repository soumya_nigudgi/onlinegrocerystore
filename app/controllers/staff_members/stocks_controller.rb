class StaffMembers::StocksController < ApplicationController
    before_action :authenticate_staff_member!
    layout 'staff_member'

    def index
        product_id = params[:product_id]
        @product = Product.find(product_id)
        @stocks = @product.stocks
    end

    def new
        product_id = params[:product_id]
        @product = Product.find(product_id)
        @stock = Stock.new
        @warehouse_locations = warehouse_id_and_locations
    end

    def create
        product_id = params[:product_id]
        @product = Product.find(product_id)
        @warehouse_locations = warehouse_id_and_locations
        @stock = @product.stocks.build(create_params)
        @stock.save
        if @stock.errors.present?
            flash[:alert] = @stock.errors.messages
            render 'staff_members/stocks/new' and return
        end
        redirect_to staff_members_product_stocks_path(@product.id)
    rescue StandardError => e
        flash[:alert] = e.message
        render 'staff_members/stocks/new' and return
    end

    def edit
        product_id = params[:product_id]
        @product = Product.find(product_id)
        @warehouse_locations = warehouse_id_and_locations
        @stock = @product.stocks.find(params[:id])
    end

    def update
        product_id = params[:product_id]
        @product = Product.find(product_id)
        @warehouse_locations = warehouse_id_and_locations
        @stock = @product.stocks.find(params[:id])
        @stock.update(create_params)
        if @stock.errors.present?
            flash[:alert] = @stock.errors.messages
            render 'staff_members/stocks/new' and return
        end
        redirect_to staff_members_product_stocks_path(@product.id)
    rescue StandardError => e
        flash[:alert] = e.message
        render 'staff_members/stocks/new' and return
    end

    def destroy
        product_id = params[:product_id]
        @product = Product.find(product_id)
        @stock = @product.stocks.find(params[:id])
        @stock.destroy
        redirect_to staff_members_product_stocks_path(@product.id)
    end

    private

    def warehouse_id_and_locations
        Warehouse.all.collect {|w| [w.address.name, w.id]}
    end

    def create_params
        params.require(:stock).permit(:quantity, :warehouse_id)
    end
end

