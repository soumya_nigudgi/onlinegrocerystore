module CustomDevise
    class RegistrationsController < Devise::RegistrationsController
        include Accessible
        skip_before_action :check_user, only: :destroy
        
        private

        def sign_up_params
            params.require(:customer).permit(:email, :encrypted_password,
                 :password, :confirm_password,:first_name, :last_name, :contact_number)
        end
    end 
end