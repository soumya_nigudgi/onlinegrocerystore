class Stock < ApplicationRecord
    belongs_to :product
    belongs_to :warehouse

    validate :validate_warehouse_capacity

    private

    def validate_warehouse_capacity
        current_warehouse_stocks = Stock.where('warehouse_id = ? and product_id != ?', self.warehouse_id, self.product_id)
        current_size = current_warehouse_stocks.map {|s| s.quantity * s.product.product_size }.sum
        new_size = current_size + (self.quantity * self.product.product_size)
        if new_size > self.warehouse.storage_capacity
            self.errors.add(:quantity, "New storage size: #{new_size} exceeds warehouse storage size: #{self.warehouse.storage_capacity}")
        end
    end
end
