require 'carrierwave/orm/activerecord'

class Product < ApplicationRecord
    mount_uploader :product_image, ProductImageUploader

    belongs_to :category
    has_many :prices
    has_many :stocks
end