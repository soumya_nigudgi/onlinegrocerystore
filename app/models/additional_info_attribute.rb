class AdditionalInfoAttribute < ApplicationRecord
    has_many :category_additional_info_attributes
    has_many :categories, through: :category_additional_info_attributes
end
