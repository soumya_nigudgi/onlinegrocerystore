class Order < ApplicationRecord
    has_many :order_products, autosave: true
    has_many :products, through: :order_products
    
    belongs_to :customer
    belongs_to :credit_card, optional: true
    belongs_to :address, optional: true

    def create_order
        order_amount = 0
        self.status = :ordered
        self.order_number = SecureRandom.hex[0..9]
        warehouses = Warehouse.joins(:address).where(addresses: {state: self.cart_shipping_state})
        self.order_products.each do|order_product|
            order_product.price = OrderProduct.price_for_cart_shipping_state(order_product.product, self.cart_shipping_state).price
            
            product_stocks = warehouses.map{|w| w.stocks.where(product_id: order_product.product_id)[0]}.compact
            if product_stocks.map(&:quantity).sum < order_product.quantity
                self.errors.add('Quantity', "#{order_product.product.product_name} available quantity is less than ordered quantity")
                return self
            end

            remaining_quantities = order_product.quantity
            product_stocks.each do|product_stock|
                if product_stock.quantity >= remaining_quantities
                    product_stock.update!(quantity: product_stock.quantity - order_product.quantity)
                    break
                else
                    remaining_quantities = remaining_quantities - product_stock.quantity
                    product_stock.update!(quantity: 0)
                end
            end
            order_amount += order_product.price * order_product.quantity
        end
        self.amount = order_amount
        self.save
        self
    end
end
