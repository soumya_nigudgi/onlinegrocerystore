class StaffMember < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :validatable

  belongs_to :address
  accepts_nested_attributes_for :address

  attr_accessor :first_name, :last_name

  before_create :update_name

  private

  def update_name  
    self.name = "(#{first_name},#{last_name})"
  end
end
