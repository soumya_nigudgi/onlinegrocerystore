class Address < ApplicationRecord
    has_one :credit_card
    has_one :staff_member
    has_many :customer_addresses
    has_many :customers, through: :customer_addresses
    has_many :orders

    attr_accessor :first_name, :last_name

    before_save :update_name

    def split_name!
        names = self.name.match(/\((.*),(.*)\)/)
        self.first_name = names[1]
        self.last_name = names[2]
    end

    private

    def update_name   
        self.name = "(#{first_name},#{last_name})"
    end
end
