class Category < ApplicationRecord
    has_many :products
    has_many :category_additional_info_attributes
    has_many :additional_info_attributes, through: :category_additional_info_attributes
end
