class Warehouse < ApplicationRecord
    belongs_to :address
    has_many :stocks
end
