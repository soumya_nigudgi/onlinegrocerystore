class Customer < ApplicationRecord
    has_many :orders
    has_many :credit_cards
    has_many :customer_addresses 
    has_many :addresses, through: :customer_addresses
    
    devise :database_authenticatable, :registerable, :validatable

    attr_accessor :first_name, :last_name

    before_create :update_name_and_status
    after_create :create_cart!

    def cart
        orders.where(status: :cart).last
    end

    def create_cart!
        return if Order.where(status: :cart, customer_id: self.id).present?
        
        Order.create!(customer_id: self.id, amount: 0.0, status: :cart, cart_shipping_state: AppConstants::STATES[0])
    end

    private

    def update_name_and_status   
        self.name = "(#{first_name},#{last_name})"
        self.status = :active
    end
end
