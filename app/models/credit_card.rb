class CreditCard < ApplicationRecord
    has_many :orders
    
    belongs_to :customer
    belongs_to :address
    accepts_nested_attributes_for :address

    validates :card_number, uniqueness: true
end
