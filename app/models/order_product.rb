class OrderProduct < ApplicationRecord
    belongs_to :order
    belongs_to :product

    alias_attribute :cart, :order

    def self.price_for_cart_shipping_state(product, cart_shipping_state)
        product.prices.find{|item| item.state == cart_shipping_state}
    end
end