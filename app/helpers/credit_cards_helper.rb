module CreditCardsHelper
    def self.mask_credit_card(card_number)
        ('x' * 12) + card_number[12..-1]
    end
end
