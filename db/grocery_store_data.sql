--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0
-- Dumped by pg_dump version 13.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: additional_info_attributes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.additional_info_attributes (id, name) VALUES (2, 'Saturated fat');
INSERT INTO public.additional_info_attributes (id, name) VALUES (3, 'Protein Cholesterol');
INSERT INTO public.additional_info_attributes (id, name) VALUES (6, 'Carbohydrates');
INSERT INTO public.additional_info_attributes (id, name) VALUES (10, 'Protein');
INSERT INTO public.additional_info_attributes (id, name) VALUES (1, 'Total Fat');
INSERT INTO public.additional_info_attributes (id, name) VALUES (4, 'Total Carbohydrate');
INSERT INTO public.additional_info_attributes (id, name) VALUES (5, 'Vitamin C');
INSERT INTO public.additional_info_attributes (id, name) VALUES (7, 'fiber');
INSERT INTO public.additional_info_attributes (id, name) VALUES (8, 'sugar');
INSERT INTO public.additional_info_attributes (id, name) VALUES (9, 'calories');


--
-- Data for Name: addresses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (1, '(Warehouse1,AL)', '333 E Oregon St', NULL, '12345', 'Chicago', 'AL', '8477974893', '2020-12-05 06:48:36.688492', '2020-12-05 06:48:36.688492');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (2, '(Warehouse2,AK)', '333 E Oregon St', NULL, '12345', 'Chicago', 'AK', '8477974893', '2020-12-05 06:48:36.723559', '2020-12-05 06:48:36.723559');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (3, '(Warehouse3,AZ)', '333 E Oregon St', NULL, '12345', 'Chicago', 'AZ', '8477974893', '2020-12-05 06:48:36.735361', '2020-12-05 06:48:36.735361');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (4, '(Warehouse4,AR)', '333 E Oregon St', NULL, '12345', 'Chicago', 'AR', '8477974893', '2020-12-05 06:48:36.745548', '2020-12-05 06:48:36.745548');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (5, '(Warehouse5,CA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'CA', '8477974893', '2020-12-05 06:48:36.755937', '2020-12-05 06:48:36.755937');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (6, '(Warehouse6,CO)', '333 E Oregon St', NULL, '12345', 'Chicago', 'CO', '8477974893', '2020-12-05 06:48:36.767983', '2020-12-05 06:48:36.767983');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (7, '(Warehouse7,CT)', '333 E Oregon St', NULL, '12345', 'Chicago', 'CT', '8477974893', '2020-12-05 06:48:36.779848', '2020-12-05 06:48:36.779848');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (8, '(Warehouse8,DE)', '333 E Oregon St', NULL, '12345', 'Chicago', 'DE', '8477974893', '2020-12-05 06:48:36.790664', '2020-12-05 06:48:36.790664');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (9, '(Warehouse9,FL)', '333 E Oregon St', NULL, '12345', 'Chicago', 'FL', '8477974893', '2020-12-05 06:48:36.801767', '2020-12-05 06:48:36.801767');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (10, '(Warehouse10,GA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'GA', '8477974893', '2020-12-05 06:48:36.811916', '2020-12-05 06:48:36.811916');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (11, '(Warehouse11,HI)', '333 E Oregon St', NULL, '12345', 'Chicago', 'HI', '8477974893', '2020-12-05 06:48:36.823398', '2020-12-05 06:48:36.823398');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (12, '(Warehouse12,ID)', '333 E Oregon St', NULL, '12345', 'Chicago', 'ID', '8477974893', '2020-12-05 06:48:36.833998', '2020-12-05 06:48:36.833998');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (13, '(Warehouse13,IL)', '333 E Oregon St', NULL, '12345', 'Chicago', 'IL', '8477974893', '2020-12-05 06:48:36.844785', '2020-12-05 06:48:36.844785');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (14, '(Warehouse14,IN)', '333 E Oregon St', NULL, '12345', 'Chicago', 'IN', '8477974893', '2020-12-05 06:48:36.855587', '2020-12-05 06:48:36.855587');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (15, '(Warehouse15,IA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'IA', '8477974893', '2020-12-05 06:48:36.866874', '2020-12-05 06:48:36.866874');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (16, '(Warehouse16,KS)', '333 E Oregon St', NULL, '12345', 'Chicago', 'KS', '8477974893', '2020-12-05 06:48:36.877288', '2020-12-05 06:48:36.877288');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (17, '(Warehouse17,KY)', '333 E Oregon St', NULL, '12345', 'Chicago', 'KY', '8477974893', '2020-12-05 06:48:36.903052', '2020-12-05 06:48:36.903052');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (18, '(Warehouse18,LA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'LA', '8477974893', '2020-12-05 06:48:36.913395', '2020-12-05 06:48:36.913395');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (19, '(Warehouse19,ME)', '333 E Oregon St', NULL, '12345', 'Chicago', 'ME', '8477974893', '2020-12-05 06:48:36.92406', '2020-12-05 06:48:36.92406');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (20, '(Warehouse20,MD)', '333 E Oregon St', NULL, '12345', 'Chicago', 'MD', '8477974893', '2020-12-05 06:48:36.934025', '2020-12-05 06:48:36.934025');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (21, '(Warehouse21,MA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'MA', '8477974893', '2020-12-05 06:48:36.943601', '2020-12-05 06:48:36.943601');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (22, '(Warehouse22,MI)', '333 E Oregon St', NULL, '12345', 'Chicago', 'MI', '8477974893', '2020-12-05 06:48:36.95408', '2020-12-05 06:48:36.95408');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (23, '(Warehouse23,MN)', '333 E Oregon St', NULL, '12345', 'Chicago', 'MN', '8477974893', '2020-12-05 06:48:36.96511', '2020-12-05 06:48:36.96511');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (24, '(Warehouse24,MS)', '333 E Oregon St', NULL, '12345', 'Chicago', 'MS', '8477974893', '2020-12-05 06:48:36.975862', '2020-12-05 06:48:36.975862');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (25, '(Warehouse25,MO)', '333 E Oregon St', NULL, '12345', 'Chicago', 'MO', '8477974893', '2020-12-05 06:48:36.987082', '2020-12-05 06:48:36.987082');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (26, '(Warehouse26,MT)', '333 E Oregon St', NULL, '12345', 'Chicago', 'MT', '8477974893', '2020-12-05 06:48:36.996502', '2020-12-05 06:48:36.996502');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (27, '(Warehouse27,NE)', '333 E Oregon St', NULL, '12345', 'Chicago', 'NE', '8477974893', '2020-12-05 06:48:37.008592', '2020-12-05 06:48:37.008592');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (28, '(Warehouse28,NV)', '333 E Oregon St', NULL, '12345', 'Chicago', 'NV', '8477974893', '2020-12-05 06:48:37.019461', '2020-12-05 06:48:37.019461');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (29, '(Warehouse29,NH)', '333 E Oregon St', NULL, '12345', 'Chicago', 'NH', '8477974893', '2020-12-05 06:48:37.030922', '2020-12-05 06:48:37.030922');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (30, '(Warehouse30,NJ)', '333 E Oregon St', NULL, '12345', 'Chicago', 'NJ', '8477974893', '2020-12-05 06:48:37.041219', '2020-12-05 06:48:37.041219');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (31, '(Warehouse31,NM)', '333 E Oregon St', NULL, '12345', 'Chicago', 'NM', '8477974893', '2020-12-05 06:48:37.051718', '2020-12-05 06:48:37.051718');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (32, '(Warehouse32,NY)', '333 E Oregon St', NULL, '12345', 'Chicago', 'NY', '8477974893', '2020-12-05 06:48:37.064498', '2020-12-05 06:48:37.064498');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (33, '(Warehouse33,NC)', '333 E Oregon St', NULL, '12345', 'Chicago', 'NC', '8477974893', '2020-12-05 06:48:37.104066', '2020-12-05 06:48:37.104066');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (34, '(Warehouse34,ND)', '333 E Oregon St', NULL, '12345', 'Chicago', 'ND', '8477974893', '2020-12-05 06:48:37.117237', '2020-12-05 06:48:37.117237');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (35, '(Warehouse35,OH)', '333 E Oregon St', NULL, '12345', 'Chicago', 'OH', '8477974893', '2020-12-05 06:48:37.132382', '2020-12-05 06:48:37.132382');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (36, '(Warehouse36,OK)', '333 E Oregon St', NULL, '12345', 'Chicago', 'OK', '8477974893', '2020-12-05 06:48:37.145135', '2020-12-05 06:48:37.145135');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (37, '(Warehouse37,OR)', '333 E Oregon St', NULL, '12345', 'Chicago', 'OR', '8477974893', '2020-12-05 06:48:37.156078', '2020-12-05 06:48:37.156078');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (38, '(Warehouse38,PA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'PA', '8477974893', '2020-12-05 06:48:37.167237', '2020-12-05 06:48:37.167237');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (39, '(Warehouse39,RI)', '333 E Oregon St', NULL, '12345', 'Chicago', 'RI', '8477974893', '2020-12-05 06:48:37.178577', '2020-12-05 06:48:37.178577');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (40, '(Warehouse40,SC)', '333 E Oregon St', NULL, '12345', 'Chicago', 'SC', '8477974893', '2020-12-05 06:48:37.190626', '2020-12-05 06:48:37.190626');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (41, '(Warehouse41,SD)', '333 E Oregon St', NULL, '12345', 'Chicago', 'SD', '8477974893', '2020-12-05 06:48:37.204381', '2020-12-05 06:48:37.204381');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (42, '(Warehouse42,TN)', '333 E Oregon St', NULL, '12345', 'Chicago', 'TN', '8477974893', '2020-12-05 06:48:37.216468', '2020-12-05 06:48:37.216468');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (43, '(Warehouse43,TX)', '333 E Oregon St', NULL, '12345', 'Chicago', 'TX', '8477974893', '2020-12-05 06:48:37.22777', '2020-12-05 06:48:37.22777');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (44, '(Warehouse44,UT)', '333 E Oregon St', NULL, '12345', 'Chicago', 'UT', '8477974893', '2020-12-05 06:48:37.238774', '2020-12-05 06:48:37.238774');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (45, '(Warehouse45,VT)', '333 E Oregon St', NULL, '12345', 'Chicago', 'VT', '8477974893', '2020-12-05 06:48:37.249632', '2020-12-05 06:48:37.249632');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (46, '(Warehouse46,VA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'VA', '8477974893', '2020-12-05 06:48:37.260507', '2020-12-05 06:48:37.260507');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (47, '(Warehouse47,WA)', '333 E Oregon St', NULL, '12345', 'Chicago', 'WA', '8477974893', '2020-12-05 06:48:37.271085', '2020-12-05 06:48:37.271085');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (48, '(Warehouse48,WV)', '333 E Oregon St', NULL, '12345', 'Chicago', 'WV', '8477974893', '2020-12-05 06:48:37.282745', '2020-12-05 06:48:37.282745');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (49, '(Warehouse49,WI)', '333 E Oregon St', NULL, '12345', 'Chicago', 'WI', '8477974893', '2020-12-05 06:48:37.293866', '2020-12-05 06:48:37.293866');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (50, '(Warehouse50,WY)', '333 E Oregon St', NULL, '12345', 'Chicago', 'WY', '8477974893', '2020-12-05 06:48:37.314577', '2020-12-05 06:48:37.314577');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (51, '(Soumya,Nigudgi)', '600 N Mcclurg Ct Apt 2305', '', '60611-4835', 'Chicago', 'IL', '0990 146 0190', '2020-12-05 06:50:44.050277', '2020-12-05 06:50:44.050277');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (52, '(Soumya,Shivsharanappa)', '600 N Mcclurg Ct Apt 2305', '', '60611-4835', 'Chicago', 'AL', '0990 146 0190', '2020-12-05 06:51:00.321374', '2020-12-05 06:51:00.321374');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (53, '(Soumya,Shivsharanappa)', '600 N Mcclurg Ct Apt 2305', '', '60611-4835', 'Chicago', 'IL', '0990 146 0190', '2020-12-05 06:51:57.509676', '2020-12-05 06:51:57.509676');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (54, '(Rachel,Steve)', 'apt 23, main road', '', '60611', 'Chicago', 'IL', '3124647893', '2020-12-06 01:52:00.051825', '2020-12-06 01:52:00.051825');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (55, '(Charli,steve)', 'apt 23, main road', '', '60611', 'Chicago', 'IL', '03124647893', '2020-12-06 02:09:37.720049', '2020-12-06 02:09:37.720049');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (56, '(Sushma,Nigudgi)', '600 N Mcclurg Ct Apt 2305', '', '6061', 'Chicago', 'AZ', '0990 146 0190', '2020-12-06 02:18:28.679006', '2020-12-06 02:18:28.679006');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (57, '(Joe,David)', '#23', 'Main Road', '657483', 'Chicago', 'IL', '3214657891', '2020-12-06 02:42:45.392572', '2020-12-06 02:42:45.392572');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (58, '(Shreyash,P)', '#243 main cross', '', '585105', 'chicago', 'IL', '9448192847', '2020-12-06 04:13:47.271818', '2020-12-06 04:13:47.271818');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (60, '(Shreyash,P)', '#243 main cross', '', '585105', 'chicago', 'AL', '9448192847', '2020-12-06 05:29:29.666624', '2020-12-06 05:29:29.666624');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (59, '(Shreyash,Patil)', '#243 main cross', '', '585105', 'chicago', 'AL', '09448192847', '2020-12-06 04:14:02.477838', '2020-12-06 05:29:46.368383');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (61, '(Phobe,williams)', '#25 E ontario', '', '60611', 'Chicago', 'IL', '9123456475', '2020-12-06 05:48:39.361093', '2020-12-06 05:48:39.361093');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (62, '(Maria,williams)', '#25 E ontario', '', '60611', 'Chicago', 'IL', '9123456475', '2020-12-06 05:51:15.128015', '2020-12-06 05:51:15.128015');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (63, '(Emma,williams)', '#25 E ontario', '', '60611', 'Chicago', 'IL', '09123456475', '2020-12-06 06:00:31.326375', '2020-12-06 06:00:31.326375');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (64, '(Harry,Potter)', '333 ontario ', '', '612435', 'New York', 'NY', '9123456789', '2020-12-06 06:06:19.790565', '2020-12-06 06:06:19.790565');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (65, '(Eva,Noah)', '333 ontario', '', '612435', 'New York', 'NY', '09123456789', '2020-12-06 06:11:51.678658', '2020-12-06 06:11:51.678658');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (67, '(Eva,Noha)', '333 ontario', '', '612435', 'Chicago', 'IL', '09123456789', '2020-12-06 06:12:59.205474', '2020-12-06 06:12:59.205474');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (68, '(Eva,Noah)', '333 ontario', '', '612435', 'New Jersey', 'NJ', '09123456789', '2020-12-06 06:14:00.481381', '2020-12-06 06:14:00.481381');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (66, '(Eva,Noah)', '333 ontario, Main street', '', '612435', 'Chicago', 'IL', '09123456789', '2020-12-06 06:12:15.448254', '2020-12-06 06:14:26.710423');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (69, '(Maria,williams)', '#25 E ontario', '', '60611', 'Chicago', 'IL', '09123456475', '2020-12-06 06:56:57.002726', '2020-12-06 06:56:57.002726');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (70, '(Sunita,williams)', '600 N ontario', '', '60611', 'Chicago', 'IL', '8147805558', '2020-12-06 07:02:03.234059', '2020-12-06 07:02:03.234059');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (71, '(Sunita,Williams)', '600 N ontario', '', '60611', 'Chicago', 'IL', '08147805558', '2020-12-06 07:02:17.08375', '2020-12-06 07:02:17.08375');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (72, '(Sunita,williams)', '600 N ontario', '', '60610', 'Chicago', 'AL', '8147805558', '2020-12-06 07:02:48.810571', '2020-12-06 07:02:48.810571');
INSERT INTO public.addresses (id, name, address_line1, address_line2, zipcode, city, state, phone_number, created_at, updated_at) VALUES (73, '(Shreyash,P)', '#243 main cross', '', '585105', 'chicago', 'IL', '09448192847', '2020-12-06 13:13:07.973913', '2020-12-06 13:13:07.973913');


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.categories (id, category_name, created_at, update_at) VALUES (1, 'Dairy', '2020-12-05 06:48:37.331418', '2020-12-05 06:48:37.331882');
INSERT INTO public.categories (id, category_name, created_at, update_at) VALUES (2, 'Breakfast', '2020-12-05 06:48:38.086335', '2020-12-05 06:48:38.08681');
INSERT INTO public.categories (id, category_name, created_at, update_at) VALUES (3, 'Pulses', '2020-12-05 06:48:38.893489', '2020-12-05 06:48:38.893878');
INSERT INTO public.categories (id, category_name, created_at, update_at) VALUES (4, 'Vegetables', '2020-12-05 06:48:39.724224', '2020-12-05 06:48:39.724561');


--
-- Data for Name: category_additional_info_attributes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (3, 2);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (3, 3);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (3, 4);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (3, 5);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (4, 6);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (4, 7);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (4, 8);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (4, 9);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (1, 1);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (1, 9);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (1, 8);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (1, 4);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (1, 10);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (2, 6);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (2, 9);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (2, 8);
INSERT INTO public.category_additional_info_attributes (category_id, additional_info_attribute_id) VALUES (2, 4);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.customers (id, name, status, email, encrypted_password, contact_number, created_at, updated_at) VALUES (1, '(Soumya,Nigudgi)', 'active', 'nigudgis@gmail.com', '$2a$12$njc1NVB8miNc6D.uEfs2Dew1q5mJtmelzoM31IP6idNjXi3zod5r2', '3124047371', '2020-12-05 06:49:45.630645', '2020-12-05 06:49:45.630645');
INSERT INTO public.customers (id, name, status, email, encrypted_password, contact_number, created_at, updated_at) VALUES (2, '(Soumya,Alur)', 'active', 'nigudgi@gmail.com', '$2a$12$4zBY3JW9G12CjABijtDbNO0LzJHw.rYLLrqHlITHga0O8TjOWmpGO', '8477974893', '2020-12-05 16:55:50.542187', '2020-12-05 16:55:50.542187');
INSERT INTO public.customers (id, name, status, email, encrypted_password, contact_number, created_at, updated_at) VALUES (3, '(Monica,Geller)', 'active', 'monica@gmail.com', '$2a$12$EYSpsXi7e77lYNy3/Z6qiuBN7Nr6.jONG9gwEWc3Lg7QVgpPMGaQ.', '8477974893', '2020-12-06 02:56:32.972441', '2020-12-06 02:56:32.972441');
INSERT INTO public.customers (id, name, status, email, encrypted_password, contact_number, created_at, updated_at) VALUES (4, '(Shreyash,P)', 'active', 'shreyash@gmail.com', '$2a$12$oPWN4pzzLuIdKaBaCEpHmOM78UOiGwD3KCcZchcBSmucFFv0c3IBm', '9448357983', '2020-12-06 04:05:57.523431', '2020-12-06 04:05:57.523431');
INSERT INTO public.customers (id, name, status, email, encrypted_password, contact_number, created_at, updated_at) VALUES (5, '(Jhon,Serene)', 'active', 'jhon1@gmail.com', '$2a$12$bgFYrlW21lm9O4od0pdaleazktiZCnqYgHq5EUebzOuDTsgbd3JK2', '81948057653', '2020-12-06 05:56:20.530265', '2020-12-06 05:56:20.530265');
INSERT INTO public.customers (id, name, status, email, encrypted_password, contact_number, created_at, updated_at) VALUES (6, '(Ava,Noah)', 'active', 'ava@gmail.com', '$2a$12$JmDEdTH35xx.NJGfaEq7reUT75GG79ibMS.y8ujPFprj09B0BstmW', '8151871111', '2020-12-06 06:09:55.949599', '2020-12-06 06:09:55.949599');
INSERT INTO public.customers (id, name, status, email, encrypted_password, contact_number, created_at, updated_at) VALUES (7, '(Sunita,Williams)', 'active', 'sunita@gmail.com', '$2a$12$LDLikqIh8HDa33jV.3XAXe0oj0lSJyfG4V8ZE6lA834X5/4BVu5.C', '9448357013', '2020-12-06 07:00:15.96478', '2020-12-06 07:00:15.96478');


--
-- Data for Name: credit_cards; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.credit_cards (id, card_number, name_on_card, customer_id, address_id, expiry_date, created_at, updated_at) VALUES (1, '11111111111111111111', 'Soumya Nigudgi', 1, 51, '09/2222', '2020-12-05 06:50:44.057346', '2020-12-05 06:50:44.057346');
INSERT INTO public.credit_cards (id, card_number, name_on_card, customer_id, address_id, expiry_date, created_at, updated_at) VALUES (2, '28394867364723123', 'Yasha', 4, 60, '12/2023', '2020-12-06 04:13:47.276918', '2020-12-06 05:29:29.676499');
INSERT INTO public.credit_cards (id, card_number, name_on_card, customer_id, address_id, expiry_date, created_at, updated_at) VALUES (4, '2134567891011121', 'Eva', 6, 67, '12/2024', '2020-12-06 06:12:59.242173', '2020-12-06 06:12:59.242173');
INSERT INTO public.credit_cards (id, card_number, name_on_card, customer_id, address_id, expiry_date, created_at, updated_at) VALUES (5, '1234567891012122', 'sunita', 7, 72, '12/2022', '2020-12-06 07:02:03.242821', '2020-12-06 07:02:48.81902');


--
-- Data for Name: customer_addresses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.customer_addresses (id, customer_id, address_id) VALUES (1, 1, 52);
INSERT INTO public.customer_addresses (id, customer_id, address_id) VALUES (2, 4, 59);
INSERT INTO public.customer_addresses (id, customer_id, address_id) VALUES (3, 6, 66);
INSERT INTO public.customer_addresses (id, customer_id, address_id) VALUES (4, 7, 71);
INSERT INTO public.customer_addresses (id, customer_id, address_id) VALUES (5, 4, 73);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (9, 'fa1bc2f11d', 4, 73, 'IL', 2, 6.70, 'ordered', NULL, '2020-12-06 05:29:05.368658', '2020-12-06 13:13:54.30336');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (1, '3cfc2c984f', 1, 52, 'AL', 1, 0.00, 'ordered', NULL, '2020-12-05 06:49:45.664896', '2020-12-05 06:51:07.698803');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (2, NULL, 1, NULL, 'AL', NULL, 0.00, 'cart', NULL, '2020-12-05 06:51:07.715013', '2020-12-05 06:51:07.715013');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (3, NULL, 2, NULL, 'AL', NULL, 0.00, 'cart', NULL, '2020-12-05 16:55:50.578465', '2020-12-05 16:55:50.578465');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (16, NULL, 4, NULL, 'AL', NULL, 0.00, 'cart', NULL, '2020-12-06 13:13:54.319839', '2020-12-06 13:13:54.319839');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (4, NULL, 3, NULL, 'AL', NULL, 0.00, 'cart', NULL, '2020-12-06 02:56:33.036391', '2020-12-06 03:04:45.84148');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (6, 'c9780f566a', 4, 59, 'IL', 2, 14.00, 'ordered', NULL, '2020-12-06 04:14:13.112267', '2020-12-06 04:39:17.810972');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (7, 'df562f3f5f', 4, 59, 'IL', 2, 68.08, 'ordered', NULL, '2020-12-06 04:39:17.844869', '2020-12-06 04:40:47.319059');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (8, '936ab77177', 4, 59, 'IL', 2, 88.08, 'ordered', NULL, '2020-12-06 04:40:47.340669', '2020-12-06 05:29:05.344461');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (10, NULL, 5, NULL, 'IL', NULL, 0.00, 'cart', NULL, '2020-12-06 05:56:20.538962', '2020-12-06 05:56:28.890435');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (11, 'f109a48490', 6, 66, 'IL', 4, 67.66, 'ordered', NULL, '2020-12-06 06:09:55.957728', '2020-12-06 06:13:16.426523');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (12, '29a72e6e78', 6, 66, 'IL', 4, 12.00, 'ordered', NULL, '2020-12-06 06:13:16.457966', '2020-12-06 06:15:48.586465');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (13, NULL, 6, NULL, 'AL', NULL, 0.00, 'cart', NULL, '2020-12-06 06:15:48.608137', '2020-12-06 06:15:48.608137');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (14, '1b24a884dd', 7, 71, 'IL', 5, 47.00, 'ordered', NULL, '2020-12-06 07:00:16.003981', '2020-12-06 07:02:21.96229');
INSERT INTO public.orders (id, order_number, customer_id, address_id, cart_shipping_state, credit_card_id, amount, status, tacking_id, created_at, updated_at) VALUES (15, NULL, 7, NULL, 'AL', NULL, 0.00, 'cart', NULL, '2020-12-06 07:02:21.980254', '2020-12-06 07:02:21.980254');


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (7, 'Potatao', 4, 'Potato, Solanum tuberosum, is an herbaceous perennial plant in the family Solanaceae which is grown for its edible tubers.', 2.50, 8.00, '"{\"Carbohydrates\":\"5.0gm\",\"fiber\":\"2gm\",\"sugar\":\"30gm\",\"calories\":\"40gm\"}"', 'potato.jpg', '2020-12-05 16:54:15.888909', '2020-12-06 00:54:41.565781');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (8, 'Oats', 2, 'Oats, (Avena sativa), domesticated cereal grass (family Poaceae) grown primarily for its edible starchy grains.', 4.00, 250.00, '"{\"Total Carbohydrate\":\"8.0%\",\"Carbohydrates\":\"2.0%\",\"sugar\":\"20gm\",\"calories\":\"330\"}"', 'oats.jpg', '2020-12-05 23:53:19.801834', '2020-12-06 01:02:10.940857');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (6, 'Whole Green Peas', 4, 'A pea is a most commonly green, occasionally golden yellow, or infrequently purple pod-shaped vegetable, widely grown as a cool-season vegetable crop', 9.00, 20.00, '"{\"Carbohydrates\":\"2gm\",\"fiber\":\"1.5gm\",\"sugar\":\"5gm\",\" calories\":\"20\"}"', 'peas.jpg', '2020-12-05 16:51:07.434041', '2020-12-06 01:03:27.668032');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (5, 'Cauliflower', 4, 'Cauliflowers are annual plants that reach about 0.5 metre (1.5 feet) tall and bear large rounded leaves that resemble collards . As desired for food, the terminal cluster forms a firm, succulent “curd,” or head. ', 6.00, 2.00, '"{\"Carbohydrates\":\"30gm\",\"fiber\":\"20gm\",\"sugar\":\"5gm\",\"calories\":\"30gm\"}"', 'cauliflower.jpg', '2020-12-05 16:48:20.922914', '2020-12-06 01:11:03.319938');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (9, 'Brown Lentil', 3, 'This is by far the most common variety of lentil.', 35.00, 250.00, '"{\"Saturated fat\":\"10gm\",\"Protein Cholesterol\":\"20\",\"Total Carbohydrate\":\"8gm\",\"Vitamin C\":\"2\"}"', 'toor-dal-1735304.jpg', '2020-12-06 01:15:02.043939', '2020-12-06 01:15:02.043939');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (10, 'Buckwheat', 3, 'Buckwheat is a fast-growing annual. The simple leaves are heart-shaped and borne along reddish hollow stems. The white flowers are produced in terminal clusters and are pollinated by bees and other insects.', 4.00, 250.00, '"{\"Saturated fat\":\"5gm\",\"Protein Cholesterol\":\"12gm\",\"Total Carbohydrate\":\"8gm\",\"Vitamin C\":\"4gm\"}"', 'buchwhaet.jpg', '2020-12-06 01:19:31.499387', '2020-12-06 01:19:31.499387');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (11, 'Kellog Corn Flakes', 2, 'Corn flakes, or cornflakes, is a breakfast cereal made from toasting flakes of corn (maize). The cereal, originally made with wheat, was created by William Kellogg in 1894 for his brother John Kellogg.', 40.00, 500.00, '"{\"Carbohydrates\":\"12gm\",\"Total Carbohydrate\":\"30gm\",\"sugar\":\"20gm\",\"calories\":\"300\"}"', 'KelloggCereals_Lead.jpg', '2020-12-06 01:23:25.277671', '2020-12-06 01:23:25.277671');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (27, 'celery', 4, 'Fresh celery from farm.', 4.00, 20.00, '"{\"Carbohydrates\":\"10gm\",\"fiber\":\"20\",\"sugar\":\"5gm\",\"calories\":\"31\"}"', 'celery.png', '2020-12-06 13:17:05.966674', '2020-12-06 13:17:05.966674');
INSERT INTO public.products (id, product_name, category_id, product_description, product_size, product_weight, additional_info, product_image, created_at, updated_at) VALUES (26, 'Carrot', 4, 'Fresh Carrots from farm.', 4.00, 20.00, '"{\"Carbohydrates\":\"5gm\",\"fiber\":\"1.2gm\",\"sugar\":\"5gm\",\"calories\":\"31\"}"', 'carrot.jpg', '2020-12-06 06:57:44.737276', '2020-12-06 06:59:13.569834');


--
-- Data for Name: order_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (38, 12, 10, 1, 12.00, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (39, 14, 26, 4, 2.00, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (41, 14, 9, 3, 13.00, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (42, 9, 7, 1, 6.70, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (8, 7, 5, 1, 8.08, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (31, 8, 5, 1, 8.08, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (34, 11, 6, 1, 6.96, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (37, 11, 5, 1, 8.08, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (33, 11, 7, 2, 6.70, 'cart');
INSERT INTO public.order_products (id, order_id, product_id, quantity, price, status) VALUES (36, 11, 8, 2, 6.61, 'cart');


--
-- Data for Name: prices; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (618, 11, 5.00, 'AL', '2020-12-06 03:27:20.400052', '2020-12-06 03:27:20.400052');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (619, 11, 10.00, 'IL', '2020-12-06 03:27:31.221118', '2020-12-06 03:27:31.221118');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (620, 10, 12.00, 'IL', '2020-12-06 03:28:05.025409', '2020-12-06 03:28:05.025409');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (621, 9, 13.00, 'IL', '2020-12-06 03:29:01.006998', '2020-12-06 03:29:01.006998');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (633, 26, 2.00, 'IL', '2020-12-06 06:57:55.190011', '2020-12-06 06:58:08.890128');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (635, 27, 10.00, 'WY', '2020-12-06 13:17:59.103282', '2020-12-06 13:17:59.103282');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (405, 5, 7.07, 'AL', '2020-12-06 00:19:32.936264', '2020-12-06 00:19:32.936264');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (406, 5, 5.57, 'AK', '2020-12-06 00:19:32.947113', '2020-12-06 00:19:32.947113');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (407, 5, 6.60, 'AZ', '2020-12-06 00:19:32.950966', '2020-12-06 00:19:32.950966');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (408, 5, 6.08, 'AR', '2020-12-06 00:19:32.998543', '2020-12-06 00:19:32.998543');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (409, 5, 8.87, 'CA', '2020-12-06 00:19:33.003878', '2020-12-06 00:19:33.003878');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (410, 5, 8.98, 'CO', '2020-12-06 00:19:33.012873', '2020-12-06 00:19:33.012873');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (411, 5, 5.05, 'CT', '2020-12-06 00:19:33.016766', '2020-12-06 00:19:33.016766');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (412, 5, 8.42, 'DE', '2020-12-06 00:19:33.020899', '2020-12-06 00:19:33.020899');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (413, 5, 9.30, 'FL', '2020-12-06 00:19:33.025704', '2020-12-06 00:19:33.025704');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (414, 5, 7.33, 'GA', '2020-12-06 00:19:33.034272', '2020-12-06 00:19:33.034272');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (415, 5, 6.76, 'HI', '2020-12-06 00:19:33.040286', '2020-12-06 00:19:33.040286');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (416, 5, 9.22, 'ID', '2020-12-06 00:19:33.044864', '2020-12-06 00:19:33.044864');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (417, 5, 8.08, 'IL', '2020-12-06 00:19:33.057981', '2020-12-06 00:19:33.057981');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (418, 5, 5.72, 'IN', '2020-12-06 00:19:33.06252', '2020-12-06 00:19:33.06252');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (419, 5, 7.35, 'IA', '2020-12-06 00:19:33.069223', '2020-12-06 00:19:33.069223');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (420, 5, 5.71, 'KS', '2020-12-06 00:19:33.073887', '2020-12-06 00:19:33.073887');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (421, 5, 6.95, 'KY', '2020-12-06 00:19:33.077962', '2020-12-06 00:19:33.077962');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (422, 5, 5.62, 'LA', '2020-12-06 00:19:33.089024', '2020-12-06 00:19:33.089024');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (423, 5, 9.75, 'ME', '2020-12-06 00:19:33.094317', '2020-12-06 00:19:33.094317');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (424, 5, 7.49, 'MD', '2020-12-06 00:19:33.098234', '2020-12-06 00:19:33.098234');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (425, 5, 6.34, 'MA', '2020-12-06 00:19:33.102233', '2020-12-06 00:19:33.102233');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (426, 5, 7.79, 'MI', '2020-12-06 00:19:33.109055', '2020-12-06 00:19:33.109055');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (427, 5, 9.16, 'MN', '2020-12-06 00:19:33.111889', '2020-12-06 00:19:33.111889');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (428, 5, 6.00, 'MS', '2020-12-06 00:19:33.114814', '2020-12-06 00:19:33.114814');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (429, 5, 8.24, 'MO', '2020-12-06 00:19:33.141323', '2020-12-06 00:19:33.141323');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (430, 5, 9.28, 'MT', '2020-12-06 00:19:33.145089', '2020-12-06 00:19:33.145089');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (431, 5, 5.82, 'NE', '2020-12-06 00:19:33.152411', '2020-12-06 00:19:33.152411');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (432, 5, 8.90, 'NV', '2020-12-06 00:19:33.156653', '2020-12-06 00:19:33.156653');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (433, 5, 6.87, 'NH', '2020-12-06 00:19:33.161425', '2020-12-06 00:19:33.161425');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (434, 5, 6.83, 'NJ', '2020-12-06 00:19:33.165555', '2020-12-06 00:19:33.165555');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (435, 5, 5.52, 'NM', '2020-12-06 00:19:33.174086', '2020-12-06 00:19:33.174086');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (436, 5, 7.31, 'NY', '2020-12-06 00:19:33.17791', '2020-12-06 00:19:33.17791');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (437, 5, 6.41, 'NC', '2020-12-06 00:19:33.181862', '2020-12-06 00:19:33.181862');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (438, 5, 6.26, 'ND', '2020-12-06 00:19:33.185906', '2020-12-06 00:19:33.185906');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (439, 5, 9.57, 'OH', '2020-12-06 00:19:33.191294', '2020-12-06 00:19:33.191294');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (440, 5, 9.10, 'OK', '2020-12-06 00:19:33.197135', '2020-12-06 00:19:33.197135');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (441, 5, 7.54, 'OR', '2020-12-06 00:19:33.201998', '2020-12-06 00:19:33.201998');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (442, 5, 5.15, 'PA', '2020-12-06 00:19:33.206415', '2020-12-06 00:19:33.206415');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (443, 5, 9.39, 'RI', '2020-12-06 00:19:33.211464', '2020-12-06 00:19:33.211464');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (444, 5, 5.21, 'SC', '2020-12-06 00:19:33.217661', '2020-12-06 00:19:33.217661');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (445, 5, 7.59, 'SD', '2020-12-06 00:19:33.22196', '2020-12-06 00:19:33.22196');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (446, 5, 6.98, 'TN', '2020-12-06 00:19:33.228114', '2020-12-06 00:19:33.228114');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (447, 5, 5.61, 'TX', '2020-12-06 00:19:33.236108', '2020-12-06 00:19:33.236108');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (448, 5, 9.47, 'UT', '2020-12-06 00:19:33.241918', '2020-12-06 00:19:33.241918');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (449, 5, 9.00, 'VT', '2020-12-06 00:19:33.253159', '2020-12-06 00:19:33.253159');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (450, 5, 6.15, 'VA', '2020-12-06 00:19:33.258982', '2020-12-06 00:19:33.258982');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (451, 5, 8.34, 'WA', '2020-12-06 00:19:33.26432', '2020-12-06 00:19:33.26432');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (452, 5, 9.91, 'WV', '2020-12-06 00:19:33.268306', '2020-12-06 00:19:33.268306');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (453, 5, 6.56, 'WI', '2020-12-06 00:19:33.273128', '2020-12-06 00:19:33.273128');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (454, 5, 7.44, 'WY', '2020-12-06 00:19:33.278918', '2020-12-06 00:19:33.278918');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (455, 6, 7.40, 'AL', '2020-12-06 00:19:33.290484', '2020-12-06 00:19:33.290484');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (456, 6, 8.11, 'AK', '2020-12-06 00:19:33.297117', '2020-12-06 00:19:33.297117');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (457, 6, 9.89, 'AZ', '2020-12-06 00:19:33.304388', '2020-12-06 00:19:33.304388');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (458, 6, 5.71, 'AR', '2020-12-06 00:19:33.310262', '2020-12-06 00:19:33.310262');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (459, 6, 7.45, 'CA', '2020-12-06 00:19:33.315727', '2020-12-06 00:19:33.315727');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (460, 6, 7.94, 'CO', '2020-12-06 00:19:33.322749', '2020-12-06 00:19:33.322749');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (461, 6, 5.51, 'CT', '2020-12-06 00:19:33.338446', '2020-12-06 00:19:33.338446');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (462, 6, 9.18, 'DE', '2020-12-06 00:19:33.341744', '2020-12-06 00:19:33.341744');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (463, 6, 5.58, 'FL', '2020-12-06 00:19:33.346783', '2020-12-06 00:19:33.346783');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (464, 6, 9.92, 'GA', '2020-12-06 00:19:33.35101', '2020-12-06 00:19:33.35101');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (465, 6, 9.71, 'HI', '2020-12-06 00:19:33.355221', '2020-12-06 00:19:33.355221');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (466, 6, 7.14, 'ID', '2020-12-06 00:19:33.360565', '2020-12-06 00:19:33.360565');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (467, 6, 6.96, 'IL', '2020-12-06 00:19:33.370487', '2020-12-06 00:19:33.370487');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (468, 6, 8.64, 'IN', '2020-12-06 00:19:33.374314', '2020-12-06 00:19:33.374314');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (469, 6, 6.45, 'IA', '2020-12-06 00:19:33.379812', '2020-12-06 00:19:33.379812');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (470, 6, 6.61, 'KS', '2020-12-06 00:19:33.383838', '2020-12-06 00:19:33.383838');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (471, 6, 8.73, 'KY', '2020-12-06 00:19:33.389431', '2020-12-06 00:19:33.389431');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (472, 6, 8.08, 'LA', '2020-12-06 00:19:33.394713', '2020-12-06 00:19:33.394713');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (473, 6, 9.51, 'ME', '2020-12-06 00:19:33.400296', '2020-12-06 00:19:33.400296');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (474, 6, 8.97, 'MD', '2020-12-06 00:19:33.405929', '2020-12-06 00:19:33.405929');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (475, 6, 7.66, 'MA', '2020-12-06 00:19:33.411781', '2020-12-06 00:19:33.411781');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (476, 6, 7.39, 'MI', '2020-12-06 00:19:33.417549', '2020-12-06 00:19:33.417549');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (477, 6, 6.92, 'MN', '2020-12-06 00:19:33.421301', '2020-12-06 00:19:33.421301');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (478, 6, 8.44, 'MS', '2020-12-06 00:19:33.429566', '2020-12-06 00:19:33.429566');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (479, 6, 6.35, 'MO', '2020-12-06 00:19:33.435774', '2020-12-06 00:19:33.435774');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (480, 6, 5.65, 'MT', '2020-12-06 00:19:33.440464', '2020-12-06 00:19:33.440464');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (481, 6, 7.57, 'NE', '2020-12-06 00:19:33.456164', '2020-12-06 00:19:33.456164');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (482, 6, 5.38, 'NV', '2020-12-06 00:19:33.462073', '2020-12-06 00:19:33.462073');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (483, 6, 6.58, 'NH', '2020-12-06 00:19:33.471453', '2020-12-06 00:19:33.471453');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (484, 6, 7.37, 'NJ', '2020-12-06 00:19:33.480667', '2020-12-06 00:19:33.480667');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (485, 6, 5.39, 'NM', '2020-12-06 00:19:33.485238', '2020-12-06 00:19:33.485238');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (486, 6, 5.33, 'NY', '2020-12-06 00:19:33.49094', '2020-12-06 00:19:33.49094');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (487, 6, 9.61, 'NC', '2020-12-06 00:19:33.497086', '2020-12-06 00:19:33.497086');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (488, 6, 7.76, 'ND', '2020-12-06 00:19:33.501791', '2020-12-06 00:19:33.501791');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (489, 6, 6.29, 'OH', '2020-12-06 00:19:33.505537', '2020-12-06 00:19:33.505537');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (490, 6, 8.64, 'OK', '2020-12-06 00:19:33.511497', '2020-12-06 00:19:33.511497');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (491, 6, 7.94, 'OR', '2020-12-06 00:19:33.519898', '2020-12-06 00:19:33.519898');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (492, 6, 6.70, 'PA', '2020-12-06 00:19:33.526649', '2020-12-06 00:19:33.526649');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (493, 6, 5.71, 'RI', '2020-12-06 00:19:33.534047', '2020-12-06 00:19:33.534047');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (494, 6, 8.57, 'SC', '2020-12-06 00:19:33.541063', '2020-12-06 00:19:33.541063');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (495, 6, 8.38, 'SD', '2020-12-06 00:19:33.546232', '2020-12-06 00:19:33.546232');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (496, 6, 9.97, 'TN', '2020-12-06 00:19:33.551857', '2020-12-06 00:19:33.551857');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (497, 6, 7.71, 'TX', '2020-12-06 00:19:33.55626', '2020-12-06 00:19:33.55626');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (498, 6, 7.00, 'UT', '2020-12-06 00:19:33.561306', '2020-12-06 00:19:33.561306');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (499, 6, 6.00, 'VT', '2020-12-06 00:19:33.577002', '2020-12-06 00:19:33.577002');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (500, 6, 5.04, 'VA', '2020-12-06 00:19:33.581473', '2020-12-06 00:19:33.581473');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (501, 6, 9.87, 'WA', '2020-12-06 00:19:33.61446', '2020-12-06 00:19:33.61446');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (502, 6, 7.12, 'WV', '2020-12-06 00:19:33.618236', '2020-12-06 00:19:33.618236');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (503, 6, 8.77, 'WI', '2020-12-06 00:19:33.623708', '2020-12-06 00:19:33.623708');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (504, 6, 9.57, 'WY', '2020-12-06 00:19:33.627247', '2020-12-06 00:19:33.627247');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (505, 7, 6.20, 'AL', '2020-12-06 00:19:33.634546', '2020-12-06 00:19:33.634546');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (506, 7, 8.70, 'AK', '2020-12-06 00:19:33.637539', '2020-12-06 00:19:33.637539');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (507, 7, 6.09, 'AZ', '2020-12-06 00:19:33.642707', '2020-12-06 00:19:33.642707');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (508, 7, 5.47, 'AR', '2020-12-06 00:19:33.648087', '2020-12-06 00:19:33.648087');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (509, 7, 5.69, 'CA', '2020-12-06 00:19:33.65207', '2020-12-06 00:19:33.65207');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (510, 7, 9.32, 'CO', '2020-12-06 00:19:33.65698', '2020-12-06 00:19:33.65698');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (511, 7, 5.26, 'CT', '2020-12-06 00:19:33.672871', '2020-12-06 00:19:33.672871');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (512, 7, 6.26, 'DE', '2020-12-06 00:19:33.676964', '2020-12-06 00:19:33.676964');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (513, 7, 8.11, 'FL', '2020-12-06 00:19:33.682772', '2020-12-06 00:19:33.682772');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (514, 7, 6.92, 'GA', '2020-12-06 00:19:33.689612', '2020-12-06 00:19:33.689612');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (515, 7, 5.24, 'HI', '2020-12-06 00:19:33.694793', '2020-12-06 00:19:33.694793');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (516, 7, 7.77, 'ID', '2020-12-06 00:19:33.704656', '2020-12-06 00:19:33.704656');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (517, 7, 6.70, 'IL', '2020-12-06 00:19:33.7094', '2020-12-06 00:19:33.7094');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (518, 7, 7.80, 'IN', '2020-12-06 00:19:33.713939', '2020-12-06 00:19:33.713939');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (519, 7, 8.80, 'IA', '2020-12-06 00:19:33.720479', '2020-12-06 00:19:33.720479');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (520, 7, 7.70, 'KS', '2020-12-06 00:19:33.724049', '2020-12-06 00:19:33.724049');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (521, 7, 5.37, 'KY', '2020-12-06 00:19:33.733254', '2020-12-06 00:19:33.733254');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (522, 7, 7.55, 'LA', '2020-12-06 00:19:33.737301', '2020-12-06 00:19:33.737301');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (523, 7, 7.38, 'ME', '2020-12-06 00:19:33.742859', '2020-12-06 00:19:33.742859');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (524, 7, 5.14, 'MD', '2020-12-06 00:19:33.751509', '2020-12-06 00:19:33.751509');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (525, 7, 5.06, 'MA', '2020-12-06 00:19:33.756406', '2020-12-06 00:19:33.756406');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (526, 7, 7.80, 'MI', '2020-12-06 00:19:33.761753', '2020-12-06 00:19:33.761753');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (527, 7, 6.61, 'MN', '2020-12-06 00:19:33.768307', '2020-12-06 00:19:33.768307');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (528, 7, 7.13, 'MS', '2020-12-06 00:19:33.772888', '2020-12-06 00:19:33.772888');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (529, 7, 6.93, 'MO', '2020-12-06 00:19:33.777915', '2020-12-06 00:19:33.777915');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (530, 7, 6.41, 'MT', '2020-12-06 00:19:33.783773', '2020-12-06 00:19:33.783773');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (531, 7, 8.58, 'NE', '2020-12-06 00:19:33.790424', '2020-12-06 00:19:33.790424');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (532, 7, 5.18, 'NV', '2020-12-06 00:19:33.796973', '2020-12-06 00:19:33.796973');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (533, 7, 9.45, 'NH', '2020-12-06 00:19:33.802595', '2020-12-06 00:19:33.802595');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (534, 7, 6.27, 'NJ', '2020-12-06 00:19:33.806624', '2020-12-06 00:19:33.806624');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (535, 7, 9.28, 'NM', '2020-12-06 00:19:33.812562', '2020-12-06 00:19:33.812562');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (536, 7, 5.88, 'NY', '2020-12-06 00:19:33.819423', '2020-12-06 00:19:33.819423');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (537, 7, 6.43, 'NC', '2020-12-06 00:19:33.8245', '2020-12-06 00:19:33.8245');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (538, 7, 5.23, 'ND', '2020-12-06 00:19:33.835411', '2020-12-06 00:19:33.835411');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (539, 7, 9.73, 'OH', '2020-12-06 00:19:33.846659', '2020-12-06 00:19:33.846659');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (540, 7, 6.86, 'OK', '2020-12-06 00:19:33.854467', '2020-12-06 00:19:33.854467');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (541, 7, 6.98, 'OR', '2020-12-06 00:19:33.860351', '2020-12-06 00:19:33.860351');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (542, 7, 6.97, 'PA', '2020-12-06 00:19:33.864132', '2020-12-06 00:19:33.864132');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (543, 7, 5.89, 'RI', '2020-12-06 00:19:33.869034', '2020-12-06 00:19:33.869034');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (544, 7, 8.19, 'SC', '2020-12-06 00:19:33.873134', '2020-12-06 00:19:33.873134');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (545, 7, 6.82, 'SD', '2020-12-06 00:19:33.878537', '2020-12-06 00:19:33.878537');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (546, 7, 9.54, 'TN', '2020-12-06 00:19:33.885083', '2020-12-06 00:19:33.885083');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (547, 7, 7.17, 'TX', '2020-12-06 00:19:33.889565', '2020-12-06 00:19:33.889565');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (548, 7, 5.93, 'UT', '2020-12-06 00:19:33.89375', '2020-12-06 00:19:33.89375');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (549, 7, 5.58, 'VT', '2020-12-06 00:19:33.900528', '2020-12-06 00:19:33.900528');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (550, 7, 8.24, 'VA', '2020-12-06 00:19:33.905412', '2020-12-06 00:19:33.905412');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (551, 7, 8.33, 'WA', '2020-12-06 00:19:33.911234', '2020-12-06 00:19:33.911234');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (552, 7, 5.93, 'WV', '2020-12-06 00:19:33.91716', '2020-12-06 00:19:33.91716');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (553, 7, 8.31, 'WI', '2020-12-06 00:19:33.921399', '2020-12-06 00:19:33.921399');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (554, 7, 5.65, 'WY', '2020-12-06 00:19:33.92597', '2020-12-06 00:19:33.92597');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (555, 8, 8.05, 'AL', '2020-12-06 00:19:33.934222', '2020-12-06 00:19:33.934222');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (556, 8, 9.42, 'AK', '2020-12-06 00:19:33.942371', '2020-12-06 00:19:33.942371');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (557, 8, 7.15, 'AZ', '2020-12-06 00:19:33.948864', '2020-12-06 00:19:33.948864');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (558, 8, 7.01, 'AR', '2020-12-06 00:19:33.95699', '2020-12-06 00:19:33.95699');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (559, 8, 8.72, 'CA', '2020-12-06 00:19:33.963964', '2020-12-06 00:19:33.963964');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (560, 8, 6.89, 'CO', '2020-12-06 00:19:33.96999', '2020-12-06 00:19:33.96999');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (561, 8, 5.74, 'CT', '2020-12-06 00:19:33.97351', '2020-12-06 00:19:33.97351');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (562, 8, 6.00, 'DE', '2020-12-06 00:19:33.979145', '2020-12-06 00:19:33.979145');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (563, 8, 8.83, 'FL', '2020-12-06 00:19:33.985745', '2020-12-06 00:19:33.985745');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (564, 8, 6.25, 'GA', '2020-12-06 00:19:33.990454', '2020-12-06 00:19:33.990454');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (565, 8, 5.55, 'HI', '2020-12-06 00:19:33.996103', '2020-12-06 00:19:33.996103');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (566, 8, 8.61, 'ID', '2020-12-06 00:19:34.002652', '2020-12-06 00:19:34.002652');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (567, 8, 6.61, 'IL', '2020-12-06 00:19:34.008068', '2020-12-06 00:19:34.008068');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (568, 8, 7.09, 'IN', '2020-12-06 00:19:34.013824', '2020-12-06 00:19:34.013824');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (569, 8, 6.50, 'IA', '2020-12-06 00:19:34.02099', '2020-12-06 00:19:34.02099');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (570, 8, 6.78, 'KS', '2020-12-06 00:19:34.024865', '2020-12-06 00:19:34.024865');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (571, 8, 5.46, 'KY', '2020-12-06 00:19:34.032574', '2020-12-06 00:19:34.032574');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (572, 8, 9.28, 'LA', '2020-12-06 00:19:34.038568', '2020-12-06 00:19:34.038568');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (573, 8, 9.23, 'ME', '2020-12-06 00:19:34.04338', '2020-12-06 00:19:34.04338');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (574, 8, 8.69, 'MD', '2020-12-06 00:19:34.054711', '2020-12-06 00:19:34.054711');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (575, 8, 5.19, 'MA', '2020-12-06 00:19:34.060159', '2020-12-06 00:19:34.060159');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (576, 8, 5.99, 'MI', '2020-12-06 00:19:34.066209', '2020-12-06 00:19:34.066209');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (577, 8, 9.41, 'MN', '2020-12-06 00:19:34.072311', '2020-12-06 00:19:34.072311');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (578, 8, 8.75, 'MS', '2020-12-06 00:19:34.088224', '2020-12-06 00:19:34.088224');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (579, 8, 6.43, 'MO', '2020-12-06 00:19:34.092756', '2020-12-06 00:19:34.092756');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (580, 8, 5.79, 'MT', '2020-12-06 00:19:34.097838', '2020-12-06 00:19:34.097838');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (581, 8, 8.25, 'NE', '2020-12-06 00:19:34.103231', '2020-12-06 00:19:34.103231');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (582, 8, 6.87, 'NV', '2020-12-06 00:19:34.107655', '2020-12-06 00:19:34.107655');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (583, 8, 6.15, 'NH', '2020-12-06 00:19:34.114182', '2020-12-06 00:19:34.114182');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (584, 8, 8.36, 'NJ', '2020-12-06 00:19:34.118757', '2020-12-06 00:19:34.118757');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (585, 8, 8.39, 'NM', '2020-12-06 00:19:34.123469', '2020-12-06 00:19:34.123469');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (586, 8, 8.59, 'NY', '2020-12-06 00:19:34.13166', '2020-12-06 00:19:34.13166');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (587, 8, 7.92, 'NC', '2020-12-06 00:19:34.13683', '2020-12-06 00:19:34.13683');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (588, 8, 6.35, 'ND', '2020-12-06 00:19:34.140339', '2020-12-06 00:19:34.140339');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (589, 8, 5.17, 'OH', '2020-12-06 00:19:34.145924', '2020-12-06 00:19:34.145924');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (590, 8, 7.42, 'OK', '2020-12-06 00:19:34.154107', '2020-12-06 00:19:34.154107');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (591, 8, 9.54, 'OR', '2020-12-06 00:19:34.159722', '2020-12-06 00:19:34.159722');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (592, 8, 6.32, 'PA', '2020-12-06 00:19:34.170009', '2020-12-06 00:19:34.170009');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (593, 8, 6.43, 'RI', '2020-12-06 00:19:34.173825', '2020-12-06 00:19:34.173825');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (594, 8, 7.95, 'SC', '2020-12-06 00:19:34.1794', '2020-12-06 00:19:34.1794');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (595, 8, 8.21, 'SD', '2020-12-06 00:19:34.18523', '2020-12-06 00:19:34.18523');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (596, 8, 9.68, 'TN', '2020-12-06 00:19:34.193225', '2020-12-06 00:19:34.193225');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (597, 8, 6.89, 'TX', '2020-12-06 00:19:34.199029', '2020-12-06 00:19:34.199029');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (598, 8, 6.69, 'UT', '2020-12-06 00:19:34.205049', '2020-12-06 00:19:34.205049');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (599, 8, 7.21, 'VT', '2020-12-06 00:19:34.214327', '2020-12-06 00:19:34.214327');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (600, 8, 6.36, 'VA', '2020-12-06 00:19:34.222315', '2020-12-06 00:19:34.222315');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (601, 8, 6.65, 'WA', '2020-12-06 00:19:34.23001', '2020-12-06 00:19:34.23001');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (602, 8, 7.69, 'WV', '2020-12-06 00:19:34.235811', '2020-12-06 00:19:34.235811');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (603, 8, 6.37, 'WI', '2020-12-06 00:19:34.240398', '2020-12-06 00:19:34.240398');
INSERT INTO public.prices (id, product_id, price, state, created_at, updated_at) VALUES (604, 8, 7.85, 'WY', '2020-12-06 00:19:34.263816', '2020-12-06 00:19:34.263816');


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: staff_members; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (1, '(Soumya,Shivsharanappa)', 'nigudgis@gmail.com', '$2a$12$oZrK5/RSa1emIcjoUCLcX.PO4lYBYJCqSOjPg/Yy7ruDUCyNtAXH2', '0990 146 0190', 53, 'store_manager', 'full_time', '2020-12-05 06:51:57.515573', '2020-12-05 06:51:57.515573');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (2, '(Rachel,steve)', 'rachel@gmail.com', '$2a$12$awUJXtl0yxGw89zXdc66muzU5QMskjllR1MtC5ip6j8evqMiMLUv2', '3124567891', 54, 'store_member', 'full_time', '2020-12-06 01:52:00.063813', '2020-12-06 01:52:00.063813');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (3, '(Charli,steve)', 'charli@gmail.com', '$2a$12$wB89xQS/xDU.QQWkqkaiceYtVD7m9NFlKt2EdKXG7mEXJhrnVOlsu', '3125648792', 55, 'store_manager', 'full_time', '2020-12-06 02:09:37.729752', '2020-12-06 02:09:37.729752');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (4, '(Sushma,Nigudgi)', 'sushma@gmail.com', '$2a$12$TG4azF6xwsXE4blZLEULteHB44YkxvSgTXFsqv/XQ4.JKvrYh8nXi', '8477974892', 56, 'store_manager', 'full_time', '2020-12-06 02:18:28.684673', '2020-12-06 02:18:28.684673');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (5, '(Joe,David)', 'joe@gmail.com', '$2a$12$jUnoofpjuDPCLzgaTZqG5eBGKLuk8aADQY14UlwD4YDh7d1OWgkyq', '3121231432', 57, 'store_manager', 'full_time', '2020-12-06 02:42:45.408167', '2020-12-06 02:42:45.408167');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (6, '(Phobe,William)', 'phobe@gmail.com', '$2a$12$Us94lDizUfblUdvgTASD.eNy040g5o4k5A6nVihm2BWe2zqwqTFI6', '9123457484', 61, 'store_member', 'part_time', '2020-12-06 05:48:39.371857', '2020-12-06 05:48:39.371857');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (8, '(Emma,williams)', 'emma@gmail.com', '$2a$12$7V5itC47C0E3TA.6/LE6c.oV9WD5HW5soi3ZHkKeAX6hPage.6tCG', '09123456475', 63, 'store_manager', 'part_time', '2020-12-06 06:00:31.365003', '2020-12-06 06:00:31.365003');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (9, '(Harry,Potter)', 'harry@gmail.com', '$2a$12$/1An.BldnvKDTDr.w9cLWuXyMQ4l.SyL6sYyyZLPocVx/s2N0pgI.', '9480576535', 64, 'store_manager', 'full_time', '2020-12-06 06:06:19.795882', '2020-12-06 06:06:19.795882');
INSERT INTO public.staff_members (id, name, email, encrypted_password, contact_number, address_id, job_title, job_type, created_at, updated_at) VALUES (10, '(Mari,williams)', 'maria@gmail.com', '$2a$12$Hcvvpee.xnGvLcymvtKaceXp4vmku3bnybOMHkmwRoehXWcftC8rS', '09123456475', 69, 'store_manager', 'full_time', '2020-12-06 06:56:57.012787', '2020-12-06 06:56:57.012787');


--
-- Data for Name: warehouses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (1, 10000.00, 1, '2020-12-05 06:48:36.712445', '2020-12-05 06:48:36.712445');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (2, 10000.00, 2, '2020-12-05 06:48:36.729974', '2020-12-05 06:48:36.729974');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (3, 10000.00, 3, '2020-12-05 06:48:36.740982', '2020-12-05 06:48:36.740982');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (4, 10000.00, 4, '2020-12-05 06:48:36.750778', '2020-12-05 06:48:36.750778');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (5, 10000.00, 5, '2020-12-05 06:48:36.762028', '2020-12-05 06:48:36.762028');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (6, 10000.00, 6, '2020-12-05 06:48:36.774313', '2020-12-05 06:48:36.774313');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (7, 10000.00, 7, '2020-12-05 06:48:36.785377', '2020-12-05 06:48:36.785377');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (8, 10000.00, 8, '2020-12-05 06:48:36.79598', '2020-12-05 06:48:36.79598');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (9, 10000.00, 9, '2020-12-05 06:48:36.806983', '2020-12-05 06:48:36.806983');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (10, 10000.00, 10, '2020-12-05 06:48:36.817075', '2020-12-05 06:48:36.817075');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (11, 10000.00, 11, '2020-12-05 06:48:36.828792', '2020-12-05 06:48:36.828792');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (12, 10000.00, 12, '2020-12-05 06:48:36.839263', '2020-12-05 06:48:36.839263');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (13, 10000.00, 13, '2020-12-05 06:48:36.850419', '2020-12-05 06:48:36.850419');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (14, 10000.00, 14, '2020-12-05 06:48:36.861807', '2020-12-05 06:48:36.861807');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (15, 10000.00, 15, '2020-12-05 06:48:36.871754', '2020-12-05 06:48:36.871754');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (16, 10000.00, 16, '2020-12-05 06:48:36.89723', '2020-12-05 06:48:36.89723');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (17, 10000.00, 17, '2020-12-05 06:48:36.908146', '2020-12-05 06:48:36.908146');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (18, 10000.00, 18, '2020-12-05 06:48:36.918899', '2020-12-05 06:48:36.918899');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (19, 10000.00, 19, '2020-12-05 06:48:36.929029', '2020-12-05 06:48:36.929029');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (20, 10000.00, 20, '2020-12-05 06:48:36.938883', '2020-12-05 06:48:36.938883');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (21, 10000.00, 21, '2020-12-05 06:48:36.948516', '2020-12-05 06:48:36.948516');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (22, 10000.00, 22, '2020-12-05 06:48:36.96012', '2020-12-05 06:48:36.96012');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (23, 10000.00, 23, '2020-12-05 06:48:36.970193', '2020-12-05 06:48:36.970193');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (24, 10000.00, 24, '2020-12-05 06:48:36.981783', '2020-12-05 06:48:36.981783');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (25, 10000.00, 25, '2020-12-05 06:48:36.991949', '2020-12-05 06:48:36.991949');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (26, 10000.00, 26, '2020-12-05 06:48:37.002406', '2020-12-05 06:48:37.002406');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (27, 10000.00, 27, '2020-12-05 06:48:37.014383', '2020-12-05 06:48:37.014383');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (28, 10000.00, 28, '2020-12-05 06:48:37.024405', '2020-12-05 06:48:37.024405');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (29, 10000.00, 29, '2020-12-05 06:48:37.035027', '2020-12-05 06:48:37.035027');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (30, 10000.00, 30, '2020-12-05 06:48:37.046172', '2020-12-05 06:48:37.046172');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (31, 10000.00, 31, '2020-12-05 06:48:37.057909', '2020-12-05 06:48:37.057909');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (32, 10000.00, 32, '2020-12-05 06:48:37.070034', '2020-12-05 06:48:37.070034');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (33, 10000.00, 33, '2020-12-05 06:48:37.109386', '2020-12-05 06:48:37.109386');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (34, 10000.00, 34, '2020-12-05 06:48:37.124747', '2020-12-05 06:48:37.124747');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (35, 10000.00, 35, '2020-12-05 06:48:37.140043', '2020-12-05 06:48:37.140043');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (36, 10000.00, 36, '2020-12-05 06:48:37.150595', '2020-12-05 06:48:37.150595');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (37, 10000.00, 37, '2020-12-05 06:48:37.162024', '2020-12-05 06:48:37.162024');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (38, 10000.00, 38, '2020-12-05 06:48:37.172967', '2020-12-05 06:48:37.172967');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (39, 10000.00, 39, '2020-12-05 06:48:37.184381', '2020-12-05 06:48:37.184381');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (40, 10000.00, 40, '2020-12-05 06:48:37.197372', '2020-12-05 06:48:37.197372');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (41, 10000.00, 41, '2020-12-05 06:48:37.210885', '2020-12-05 06:48:37.210885');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (42, 10000.00, 42, '2020-12-05 06:48:37.222567', '2020-12-05 06:48:37.222567');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (43, 10000.00, 43, '2020-12-05 06:48:37.232933', '2020-12-05 06:48:37.232933');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (44, 10000.00, 44, '2020-12-05 06:48:37.244224', '2020-12-05 06:48:37.244224');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (45, 10000.00, 45, '2020-12-05 06:48:37.255108', '2020-12-05 06:48:37.255108');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (46, 10000.00, 46, '2020-12-05 06:48:37.265695', '2020-12-05 06:48:37.265695');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (47, 10000.00, 47, '2020-12-05 06:48:37.276572', '2020-12-05 06:48:37.276572');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (48, 10000.00, 48, '2020-12-05 06:48:37.288448', '2020-12-05 06:48:37.288448');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (49, 10000.00, 49, '2020-12-05 06:48:37.298933', '2020-12-05 06:48:37.298933');
INSERT INTO public.warehouses (id, storage_capacity, address_id, created_at, updated_at) VALUES (50, 10000.00, 50, '2020-12-05 06:48:37.31987', '2020-12-05 06:48:37.31987');


--
-- Data for Name: stocks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (617, 11, 11, 13, '2020-12-06 03:27:41.511272', '2020-12-06 04:14:13.052127');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (635, 3, 5, 13, '2020-12-06 13:15:14.805502', '2020-12-06 13:15:14.805502');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (636, 10, 27, 13, '2020-12-06 13:17:23.808241', '2020-12-06 13:17:23.808241');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (637, 10, 27, 50, '2020-12-06 13:17:35.388959', '2020-12-06 13:17:35.388959');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (638, 10, 27, 7, '2020-12-06 13:17:42.113191', '2020-12-06 13:17:42.113191');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (466, 3, 6, 13, '2020-12-06 00:16:16.007031', '2020-12-06 06:13:16.232376');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (633, 16, 26, 13, '2020-12-06 06:58:28.729638', '2020-12-06 07:02:21.929075');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (404, 5, 5, 1, '2020-12-06 00:16:14.882456', '2020-12-06 00:16:14.882456');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (405, 5, 5, 2, '2020-12-06 00:16:14.897963', '2020-12-06 00:16:14.897963');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (406, 5, 5, 3, '2020-12-06 00:16:14.91588', '2020-12-06 00:16:14.91588');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (407, 5, 5, 4, '2020-12-06 00:16:14.939951', '2020-12-06 00:16:14.939951');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (408, 5, 5, 5, '2020-12-06 00:16:14.959837', '2020-12-06 00:16:14.959837');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (409, 5, 5, 6, '2020-12-06 00:16:14.973119', '2020-12-06 00:16:14.973119');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (410, 5, 5, 7, '2020-12-06 00:16:14.985822', '2020-12-06 00:16:14.985822');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (411, 5, 5, 8, '2020-12-06 00:16:15.005078', '2020-12-06 00:16:15.005078');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (412, 5, 5, 9, '2020-12-06 00:16:15.019215', '2020-12-06 00:16:15.019215');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (413, 5, 5, 10, '2020-12-06 00:16:15.034425', '2020-12-06 00:16:15.034425');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (414, 5, 5, 11, '2020-12-06 00:16:15.059473', '2020-12-06 00:16:15.059473');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (415, 5, 5, 12, '2020-12-06 00:16:15.072993', '2020-12-06 00:16:15.072993');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (417, 5, 5, 14, '2020-12-06 00:16:15.108057', '2020-12-06 00:16:15.108057');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (418, 5, 5, 15, '2020-12-06 00:16:15.134918', '2020-12-06 00:16:15.134918');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (419, 5, 5, 16, '2020-12-06 00:16:15.152706', '2020-12-06 00:16:15.152706');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (420, 5, 5, 17, '2020-12-06 00:16:15.168173', '2020-12-06 00:16:15.168173');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (421, 5, 5, 18, '2020-12-06 00:16:15.183412', '2020-12-06 00:16:15.183412');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (422, 5, 5, 19, '2020-12-06 00:16:15.199708', '2020-12-06 00:16:15.199708');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (423, 5, 5, 20, '2020-12-06 00:16:15.218155', '2020-12-06 00:16:15.218155');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (424, 5, 5, 21, '2020-12-06 00:16:15.250718', '2020-12-06 00:16:15.250718');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (425, 5, 5, 22, '2020-12-06 00:16:15.269192', '2020-12-06 00:16:15.269192');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (426, 5, 5, 23, '2020-12-06 00:16:15.292593', '2020-12-06 00:16:15.292593');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (427, 5, 5, 24, '2020-12-06 00:16:15.306615', '2020-12-06 00:16:15.306615');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (428, 5, 5, 25, '2020-12-06 00:16:15.317069', '2020-12-06 00:16:15.317069');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (429, 5, 5, 26, '2020-12-06 00:16:15.332644', '2020-12-06 00:16:15.332644');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (430, 5, 5, 27, '2020-12-06 00:16:15.358371', '2020-12-06 00:16:15.358371');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (431, 5, 5, 28, '2020-12-06 00:16:15.372276', '2020-12-06 00:16:15.372276');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (432, 5, 5, 29, '2020-12-06 00:16:15.382372', '2020-12-06 00:16:15.382372');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (433, 5, 5, 30, '2020-12-06 00:16:15.402292', '2020-12-06 00:16:15.402292');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (434, 5, 5, 31, '2020-12-06 00:16:15.421683', '2020-12-06 00:16:15.421683');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (435, 5, 5, 32, '2020-12-06 00:16:15.440718', '2020-12-06 00:16:15.440718');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (436, 5, 5, 33, '2020-12-06 00:16:15.45584', '2020-12-06 00:16:15.45584');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (437, 5, 5, 34, '2020-12-06 00:16:15.467404', '2020-12-06 00:16:15.467404');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (438, 5, 5, 35, '2020-12-06 00:16:15.484734', '2020-12-06 00:16:15.484734');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (439, 5, 5, 36, '2020-12-06 00:16:15.495397', '2020-12-06 00:16:15.495397');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (440, 5, 5, 37, '2020-12-06 00:16:15.51161', '2020-12-06 00:16:15.51161');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (441, 5, 5, 38, '2020-12-06 00:16:15.525191', '2020-12-06 00:16:15.525191');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (442, 5, 5, 39, '2020-12-06 00:16:15.541143', '2020-12-06 00:16:15.541143');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (443, 5, 5, 40, '2020-12-06 00:16:15.558976', '2020-12-06 00:16:15.558976');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (444, 5, 5, 41, '2020-12-06 00:16:15.574908', '2020-12-06 00:16:15.574908');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (445, 5, 5, 42, '2020-12-06 00:16:15.592957', '2020-12-06 00:16:15.592957');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (446, 5, 5, 43, '2020-12-06 00:16:15.612319', '2020-12-06 00:16:15.612319');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (447, 5, 5, 44, '2020-12-06 00:16:15.622871', '2020-12-06 00:16:15.622871');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (448, 5, 5, 45, '2020-12-06 00:16:15.632309', '2020-12-06 00:16:15.632309');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (449, 5, 5, 46, '2020-12-06 00:16:15.652563', '2020-12-06 00:16:15.652563');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (450, 5, 5, 47, '2020-12-06 00:16:15.66719', '2020-12-06 00:16:15.66719');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (451, 5, 5, 48, '2020-12-06 00:16:15.689113', '2020-12-06 00:16:15.689113');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (452, 5, 5, 49, '2020-12-06 00:16:15.701705', '2020-12-06 00:16:15.701705');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (453, 5, 5, 50, '2020-12-06 00:16:15.715711', '2020-12-06 00:16:15.715711');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (454, 5, 6, 1, '2020-12-06 00:16:15.742978', '2020-12-06 00:16:15.742978');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (455, 5, 6, 2, '2020-12-06 00:16:15.774564', '2020-12-06 00:16:15.774564');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (456, 5, 6, 3, '2020-12-06 00:16:15.792696', '2020-12-06 00:16:15.792696');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (457, 5, 6, 4, '2020-12-06 00:16:15.807961', '2020-12-06 00:16:15.807961');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (458, 5, 6, 5, '2020-12-06 00:16:15.830221', '2020-12-06 00:16:15.830221');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (459, 5, 6, 6, '2020-12-06 00:16:15.860478', '2020-12-06 00:16:15.860478');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (460, 5, 6, 7, '2020-12-06 00:16:15.890464', '2020-12-06 00:16:15.890464');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (461, 5, 6, 8, '2020-12-06 00:16:15.913341', '2020-12-06 00:16:15.913341');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (462, 5, 6, 9, '2020-12-06 00:16:15.929826', '2020-12-06 00:16:15.929826');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (463, 5, 6, 10, '2020-12-06 00:16:15.946781', '2020-12-06 00:16:15.946781');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (464, 5, 6, 11, '2020-12-06 00:16:15.972258', '2020-12-06 00:16:15.972258');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (465, 5, 6, 12, '2020-12-06 00:16:15.986845', '2020-12-06 00:16:15.986845');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (467, 5, 6, 14, '2020-12-06 00:16:16.026203', '2020-12-06 00:16:16.026203');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (468, 5, 6, 15, '2020-12-06 00:16:16.046494', '2020-12-06 00:16:16.046494');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (469, 5, 6, 16, '2020-12-06 00:16:16.059914', '2020-12-06 00:16:16.059914');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (470, 5, 6, 17, '2020-12-06 00:16:16.081454', '2020-12-06 00:16:16.081454');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (471, 5, 6, 18, '2020-12-06 00:16:16.098776', '2020-12-06 00:16:16.098776');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (472, 5, 6, 19, '2020-12-06 00:16:16.115614', '2020-12-06 00:16:16.115614');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (473, 5, 6, 20, '2020-12-06 00:16:16.167024', '2020-12-06 00:16:16.167024');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (474, 5, 6, 21, '2020-12-06 00:16:16.177941', '2020-12-06 00:16:16.177941');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (475, 5, 6, 22, '2020-12-06 00:16:16.194666', '2020-12-06 00:16:16.194666');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (476, 5, 6, 23, '2020-12-06 00:16:16.20833', '2020-12-06 00:16:16.20833');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (477, 5, 6, 24, '2020-12-06 00:16:16.227702', '2020-12-06 00:16:16.227702');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (478, 5, 6, 25, '2020-12-06 00:16:16.248418', '2020-12-06 00:16:16.248418');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (479, 5, 6, 26, '2020-12-06 00:16:16.260876', '2020-12-06 00:16:16.260876');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (480, 5, 6, 27, '2020-12-06 00:16:16.284466', '2020-12-06 00:16:16.284466');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (481, 5, 6, 28, '2020-12-06 00:16:16.301681', '2020-12-06 00:16:16.301681');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (482, 5, 6, 29, '2020-12-06 00:16:16.319194', '2020-12-06 00:16:16.319194');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (483, 5, 6, 30, '2020-12-06 00:16:16.335876', '2020-12-06 00:16:16.335876');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (484, 5, 6, 31, '2020-12-06 00:16:16.365352', '2020-12-06 00:16:16.365352');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (485, 5, 6, 32, '2020-12-06 00:16:16.378273', '2020-12-06 00:16:16.378273');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (486, 5, 6, 33, '2020-12-06 00:16:16.395667', '2020-12-06 00:16:16.395667');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (487, 5, 6, 34, '2020-12-06 00:16:16.416598', '2020-12-06 00:16:16.416598');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (488, 5, 6, 35, '2020-12-06 00:16:16.435528', '2020-12-06 00:16:16.435528');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (489, 5, 6, 36, '2020-12-06 00:16:16.453063', '2020-12-06 00:16:16.453063');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (490, 5, 6, 37, '2020-12-06 00:16:16.467639', '2020-12-06 00:16:16.467639');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (491, 5, 6, 38, '2020-12-06 00:16:16.486443', '2020-12-06 00:16:16.486443');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (492, 5, 6, 39, '2020-12-06 00:16:16.500887', '2020-12-06 00:16:16.500887');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (493, 5, 6, 40, '2020-12-06 00:16:16.517694', '2020-12-06 00:16:16.517694');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (494, 5, 6, 41, '2020-12-06 00:16:16.53534', '2020-12-06 00:16:16.53534');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (495, 5, 6, 42, '2020-12-06 00:16:16.583073', '2020-12-06 00:16:16.583073');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (496, 5, 6, 43, '2020-12-06 00:16:16.601202', '2020-12-06 00:16:16.601202');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (497, 5, 6, 44, '2020-12-06 00:16:16.616366', '2020-12-06 00:16:16.616366');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (498, 5, 6, 45, '2020-12-06 00:16:16.633998', '2020-12-06 00:16:16.633998');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (499, 5, 6, 46, '2020-12-06 00:16:16.648586', '2020-12-06 00:16:16.648586');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (500, 5, 6, 47, '2020-12-06 00:16:16.665059', '2020-12-06 00:16:16.665059');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (501, 5, 6, 48, '2020-12-06 00:16:16.683259', '2020-12-06 00:16:16.683259');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (502, 5, 6, 49, '2020-12-06 00:16:16.708252', '2020-12-06 00:16:16.708252');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (503, 5, 6, 50, '2020-12-06 00:16:16.731906', '2020-12-06 00:16:16.731906');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (504, 5, 7, 1, '2020-12-06 00:16:16.747055', '2020-12-06 00:16:16.747055');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (505, 5, 7, 2, '2020-12-06 00:16:16.766491', '2020-12-06 00:16:16.766491');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (506, 5, 7, 3, '2020-12-06 00:16:16.785322', '2020-12-06 00:16:16.785322');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (507, 5, 7, 4, '2020-12-06 00:16:16.801548', '2020-12-06 00:16:16.801548');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (508, 5, 7, 5, '2020-12-06 00:16:16.831709', '2020-12-06 00:16:16.831709');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (509, 5, 7, 6, '2020-12-06 00:16:16.854839', '2020-12-06 00:16:16.854839');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (510, 5, 7, 7, '2020-12-06 00:16:16.869695', '2020-12-06 00:16:16.869695');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (511, 5, 7, 8, '2020-12-06 00:16:16.895595', '2020-12-06 00:16:16.895595');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (512, 5, 7, 9, '2020-12-06 00:16:16.926', '2020-12-06 00:16:16.926');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (513, 5, 7, 10, '2020-12-06 00:16:16.949606', '2020-12-06 00:16:16.949606');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (514, 5, 7, 11, '2020-12-06 00:16:16.968173', '2020-12-06 00:16:16.968173');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (515, 5, 7, 12, '2020-12-06 00:16:16.988953', '2020-12-06 00:16:16.988953');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (517, 5, 7, 14, '2020-12-06 00:16:17.046262', '2020-12-06 00:16:17.046262');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (518, 5, 7, 15, '2020-12-06 00:16:17.060003', '2020-12-06 00:16:17.060003');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (519, 5, 7, 16, '2020-12-06 00:16:17.080674', '2020-12-06 00:16:17.080674');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (520, 5, 7, 17, '2020-12-06 00:16:17.096701', '2020-12-06 00:16:17.096701');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (521, 5, 7, 18, '2020-12-06 00:16:17.12882', '2020-12-06 00:16:17.12882');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (522, 5, 7, 19, '2020-12-06 00:16:17.140653', '2020-12-06 00:16:17.140653');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (523, 5, 7, 20, '2020-12-06 00:16:17.163559', '2020-12-06 00:16:17.163559');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (524, 5, 7, 21, '2020-12-06 00:16:17.178793', '2020-12-06 00:16:17.178793');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (525, 5, 7, 22, '2020-12-06 00:16:17.203396', '2020-12-06 00:16:17.203396');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (526, 5, 7, 23, '2020-12-06 00:16:17.22232', '2020-12-06 00:16:17.22232');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (527, 5, 7, 24, '2020-12-06 00:16:17.250242', '2020-12-06 00:16:17.250242');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (528, 5, 7, 25, '2020-12-06 00:16:17.268015', '2020-12-06 00:16:17.268015');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (529, 5, 7, 26, '2020-12-06 00:16:17.291998', '2020-12-06 00:16:17.291998');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (530, 5, 7, 27, '2020-12-06 00:16:17.313031', '2020-12-06 00:16:17.313031');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (531, 5, 7, 28, '2020-12-06 00:16:17.335521', '2020-12-06 00:16:17.335521');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (532, 5, 7, 29, '2020-12-06 00:16:17.353491', '2020-12-06 00:16:17.353491');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (533, 5, 7, 30, '2020-12-06 00:16:17.377919', '2020-12-06 00:16:17.377919');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (534, 5, 7, 31, '2020-12-06 00:16:17.399992', '2020-12-06 00:16:17.399992');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (535, 5, 7, 32, '2020-12-06 00:16:17.420147', '2020-12-06 00:16:17.420147');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (536, 5, 7, 33, '2020-12-06 00:16:17.435419', '2020-12-06 00:16:17.435419');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (537, 5, 7, 34, '2020-12-06 00:16:17.460737', '2020-12-06 00:16:17.460737');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (538, 5, 7, 35, '2020-12-06 00:16:17.480861', '2020-12-06 00:16:17.480861');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (539, 5, 7, 36, '2020-12-06 00:16:17.498979', '2020-12-06 00:16:17.498979');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (540, 5, 7, 37, '2020-12-06 00:16:17.516638', '2020-12-06 00:16:17.516638');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (541, 5, 7, 38, '2020-12-06 00:16:17.543295', '2020-12-06 00:16:17.543295');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (542, 5, 7, 39, '2020-12-06 00:16:17.573042', '2020-12-06 00:16:17.573042');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (543, 5, 7, 40, '2020-12-06 00:16:17.589118', '2020-12-06 00:16:17.589118');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (544, 5, 7, 41, '2020-12-06 00:16:17.615049', '2020-12-06 00:16:17.615049');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (545, 5, 7, 42, '2020-12-06 00:16:17.640706', '2020-12-06 00:16:17.640706');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (546, 5, 7, 43, '2020-12-06 00:16:17.660749', '2020-12-06 00:16:17.660749');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (547, 5, 7, 44, '2020-12-06 00:16:17.683771', '2020-12-06 00:16:17.683771');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (548, 5, 7, 45, '2020-12-06 00:16:17.697177', '2020-12-06 00:16:17.697177');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (549, 5, 7, 46, '2020-12-06 00:16:17.717017', '2020-12-06 00:16:17.717017');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (550, 5, 7, 47, '2020-12-06 00:16:17.739789', '2020-12-06 00:16:17.739789');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (551, 5, 7, 48, '2020-12-06 00:16:17.764626', '2020-12-06 00:16:17.764626');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (552, 5, 7, 49, '2020-12-06 00:16:17.788013', '2020-12-06 00:16:17.788013');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (553, 5, 7, 50, '2020-12-06 00:16:17.813445', '2020-12-06 00:16:17.813445');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (554, 5, 8, 1, '2020-12-06 00:16:17.831884', '2020-12-06 00:16:17.831884');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (555, 5, 8, 2, '2020-12-06 00:16:17.863981', '2020-12-06 00:16:17.863981');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (556, 5, 8, 3, '2020-12-06 00:16:17.882929', '2020-12-06 00:16:17.882929');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (557, 5, 8, 4, '2020-12-06 00:16:17.902006', '2020-12-06 00:16:17.902006');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (558, 5, 8, 5, '2020-12-06 00:16:17.923288', '2020-12-06 00:16:17.923288');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (559, 5, 8, 6, '2020-12-06 00:16:17.942698', '2020-12-06 00:16:17.942698');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (560, 5, 8, 7, '2020-12-06 00:16:17.967358', '2020-12-06 00:16:17.967358');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (561, 5, 8, 8, '2020-12-06 00:16:17.98217', '2020-12-06 00:16:17.98217');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (562, 5, 8, 9, '2020-12-06 00:16:18.00628', '2020-12-06 00:16:18.00628');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (563, 5, 8, 10, '2020-12-06 00:16:18.059192', '2020-12-06 00:16:18.059192');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (564, 5, 8, 11, '2020-12-06 00:16:18.084073', '2020-12-06 00:16:18.084073');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (565, 5, 8, 12, '2020-12-06 00:16:18.10669', '2020-12-06 00:16:18.10669');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (567, 5, 8, 14, '2020-12-06 00:16:18.158415', '2020-12-06 00:16:18.158415');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (568, 5, 8, 15, '2020-12-06 00:16:18.174214', '2020-12-06 00:16:18.174214');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (569, 5, 8, 16, '2020-12-06 00:16:18.194701', '2020-12-06 00:16:18.194701');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (570, 5, 8, 17, '2020-12-06 00:16:18.212991', '2020-12-06 00:16:18.212991');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (571, 5, 8, 18, '2020-12-06 00:16:18.232951', '2020-12-06 00:16:18.232951');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (572, 5, 8, 19, '2020-12-06 00:16:18.254946', '2020-12-06 00:16:18.254946');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (573, 5, 8, 20, '2020-12-06 00:16:18.284323', '2020-12-06 00:16:18.284323');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (574, 5, 8, 21, '2020-12-06 00:16:18.300678', '2020-12-06 00:16:18.300678');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (575, 5, 8, 22, '2020-12-06 00:16:18.31905', '2020-12-06 00:16:18.31905');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (576, 5, 8, 23, '2020-12-06 00:16:18.34147', '2020-12-06 00:16:18.34147');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (577, 5, 8, 24, '2020-12-06 00:16:18.363528', '2020-12-06 00:16:18.363528');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (578, 5, 8, 25, '2020-12-06 00:16:18.39036', '2020-12-06 00:16:18.39036');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (579, 5, 8, 26, '2020-12-06 00:16:18.408752', '2020-12-06 00:16:18.408752');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (580, 5, 8, 27, '2020-12-06 00:16:18.427969', '2020-12-06 00:16:18.427969');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (581, 5, 8, 28, '2020-12-06 00:16:18.445896', '2020-12-06 00:16:18.445896');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (582, 5, 8, 29, '2020-12-06 00:16:18.464048', '2020-12-06 00:16:18.464048');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (583, 5, 8, 30, '2020-12-06 00:16:18.489173', '2020-12-06 00:16:18.489173');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (584, 5, 8, 31, '2020-12-06 00:16:18.516165', '2020-12-06 00:16:18.516165');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (585, 5, 8, 32, '2020-12-06 00:16:18.533276', '2020-12-06 00:16:18.533276');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (586, 5, 8, 33, '2020-12-06 00:16:18.559114', '2020-12-06 00:16:18.559114');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (587, 5, 8, 34, '2020-12-06 00:16:18.586701', '2020-12-06 00:16:18.586701');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (588, 5, 8, 35, '2020-12-06 00:16:18.612954', '2020-12-06 00:16:18.612954');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (589, 5, 8, 36, '2020-12-06 00:16:18.632552', '2020-12-06 00:16:18.632552');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (590, 5, 8, 37, '2020-12-06 00:16:18.662981', '2020-12-06 00:16:18.662981');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (591, 5, 8, 38, '2020-12-06 00:16:18.689237', '2020-12-06 00:16:18.689237');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (592, 5, 8, 39, '2020-12-06 00:16:18.705869', '2020-12-06 00:16:18.705869');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (593, 5, 8, 40, '2020-12-06 00:16:18.725229', '2020-12-06 00:16:18.725229');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (594, 5, 8, 41, '2020-12-06 00:16:18.74354', '2020-12-06 00:16:18.74354');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (595, 5, 8, 42, '2020-12-06 00:16:18.784051', '2020-12-06 00:16:18.784051');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (596, 5, 8, 43, '2020-12-06 00:16:18.813094', '2020-12-06 00:16:18.813094');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (597, 5, 8, 44, '2020-12-06 00:16:18.834119', '2020-12-06 00:16:18.834119');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (598, 5, 8, 45, '2020-12-06 00:16:18.858496', '2020-12-06 00:16:18.858496');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (599, 5, 8, 46, '2020-12-06 00:16:18.878654', '2020-12-06 00:16:18.878654');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (600, 5, 8, 47, '2020-12-06 00:16:18.90233', '2020-12-06 00:16:18.90233');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (601, 5, 8, 48, '2020-12-06 00:16:18.926986', '2020-12-06 00:16:18.926986');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (602, 5, 8, 49, '2020-12-06 00:16:18.946677', '2020-12-06 00:16:18.946677');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (603, 5, 8, 50, '2020-12-06 00:16:18.965497', '2020-12-06 00:16:18.965497');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (618, 10, 10, 1, '2020-12-06 03:28:13.77396', '2020-12-06 03:28:13.77396');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (516, 0, 7, 13, '2020-12-06 00:16:17.007543', '2020-12-06 13:13:54.298937');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (620, 0, 9, 13, '2020-12-06 03:29:13.256403', '2020-12-06 07:02:21.956537');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (566, 3, 8, 13, '2020-12-06 00:16:18.129854', '2020-12-06 06:13:16.421037');
INSERT INTO public.stocks (id, quantity, product_id, warehouse_id, created_at, updated_at) VALUES (619, 11, 10, 13, '2020-12-06 03:28:25.423578', '2020-12-06 06:15:48.580727');


--
-- Data for Name: suppliers; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: supplier_products; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: additional_info_attributes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.additional_info_attributes_id_seq', 10, true);


--
-- Name: addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.addresses_id_seq', 73, true);


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_id_seq', 4, true);


--
-- Name: credit_cards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.credit_cards_id_seq', 5, true);


--
-- Name: customer_addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customer_addresses_id_seq', 5, true);


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customers_id_seq', 7, true);


--
-- Name: order_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_products_id_seq', 43, true);


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 16, true);


--
-- Name: prices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.prices_id_seq', 635, true);


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 27, true);


--
-- Name: staff_members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.staff_members_id_seq', 10, true);


--
-- Name: stocks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.stocks_id_seq', 638, true);


--
-- Name: suppliers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.suppliers_id_seq', 1, false);


--
-- Name: warehouses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.warehouses_id_seq', 50, true);


--
-- PostgreSQL database dump complete
--

