# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def add_prices(product)
    AppConstants::STATES.each do|state|
        product.prices.create!(price: rand(1.0..100.0), state: state)
    end
end

def add_stock(product)
    Warehouse.all.each do|warehouse|
        product.stocks.create!(warehouse: warehouse, quantity: 10)
    end
end

Price.delete_all
Product.destroy_all
Category.destroy_all
Stock.destroy_all
Warehouse.destroy_all


AppConstants::STATES.each_with_index do|state, index|
    address = Address.create(first_name: "Warehouse#{index + 1}", last_name: state, address_line1: '333 E Oregon St',
     city: 'Chicago', state: state, zipcode: '12345', phone_number: '8477974893')
    Warehouse.create(address: address, storage_capacity: 10000)
end

['Dairy', 'Breakfast', 'Pulses', 'Vegetables'].each do|name|
    category = Category.create(category_name: name)
    1.times do |index_2|
        product = Product.create(
            product_name: "Product #{category.category_name}-#{index_2 + 1}",
            product_description: 'Test product',
            category: category,
            product_size: 3
        )
        add_prices(product)
        add_stock(product)
    end
end
