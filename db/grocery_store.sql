CREATE TYPE full_name AS (
    first_name          VARCHAR(50),
    last_name           VARCHAR(50)
    );
CREATE TYPE customer_status AS enum ('active', 'inactive');
CREATE TABLE customers(
    id                  SERIAL,
    name                full_name NOT NULL,
    status              customer_status NOT NULL,
    email               VARCHAR(50) NOT NULL UNIQUE,
    encrypted_password  VARCHAR(255) NOT NULL,
    contact_number      VARCHAR(14) NOT NULL,
    created_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
    );


CREATE TABLE addresses(
    id                  SERIAL,
    name                full_name NOT NULL,
    address_line1       VARCHAR(255) NOT NULL,
    address_line2       VARCHAR(255),
    zipcode             VARCHAR(10) NOT NULL,
    city                VARCHAR(50) NOT NULL,
    state               CHAR(2) NOT NULL,
    phone_number        VARCHAR(14) NOT NULL,
    created_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at           TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
    );

CREATE TABLE customer_addresses(
    id                 SERIAL,
    customer_id        INTEGER NOT NULL,
    address_id         INTEGER NOT NULL,
    PRIMARY KEY(id),

    FOREIGN KEY (customer_id) references customers(id)
    ON DELETE cascade,
    FOREIGN KEY (address_id) references addresses(id)
    ON DELETE cascade,
    UNIQUE (customer_id,address_id)
    );

CREATE TABLE credit_cards(
    id                      SERIAL,
    card_number             VARCHAR(20) NOT NULL UNIQUE,
    name_on_card            VARCHAR(26) NOT NULL,
    customer_id             INTEGER NOT NULL,
    address_id              INTEGER NOT NULL,
    expiry_date             CHAR(7) NOT NULL,
    created_at              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY (customer_id) references customers(id)
    ON DELETE CASCADE,
    FOREIGN KEY(address_id) references addresses(id)
    ON DELETE SET NULL
);
CREATE INDEX credit_cards_customer_id_key ON credit_cards (customer_id);

CREATE TABLE warehouses(
    id                  SERIAL,
    storage_capacity    NUMERIC(10,2) NOT NULL CHECK (storage_capacity > 0.0),
    address_id          INTEGER,
    created_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY(address_id) references addresses(id)
    ON DELETE SET NULL
);


CREATE TABLE suppliers(
    id                  SERIAL,
    name                full_name NOT NULL,
    address_id          INTEGER NOT NULL,
    created_at          TIMESTAMP  DEFAULT CURRENT_TIMESTAMP,
    updated_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY (address_id) references addresses(id)
    ON DELETE cascade
);

CREATE TABLE categories(
    id                SERIAL,
    category_name     VARCHAR(50) NOT NULL UNIQUE,
    created_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    update_at        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
);

CREATE TABLE products(
    id                  SERIAL,
    product_name        VARCHAR(255) NOT NULL,
    category_id         INTEGER, 
    product_description TEXT NOT NULL,
    product_size        NUMERIC(10,2) CHECK(product_size > 0.0),
    product_weight      NUMERIC(10,2) CHECK(product_weight > 0.0),
    additional_info     JSON,
    product_image       VARCHAR(255),
    created_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY(category_id) references categories(id) 
    ON DELETE SET NULL
);
CREATE INDEX products_category_id_key ON products (category_id);

CREATE TABLE prices(
    id              SERIAL,
    product_id      INTEGER NOT NULL,
    price           NUMERIC(10,2) NOT NULL CHECK(price > 0.0),
    state           CHAR(2) NOT NULL,
    created_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY(product_id) references products(id) 
    ON DELETE CASCADE,
    UNIQUE (product_id,state)
);

CREATE TABLE supplier_products(
    supplier_id            INTEGER NOT NULL,
    product_id             INTEGER NOT NULL,
    price                  NUMERIC(10,2) NOT NULL CHECK(price > 0.0),
    FOREIGN KEY (supplier_id) references suppliers(id) 
    ON DELETE cascade,
    FOREIGN KEY (product_id) references products(id)
    ON DELETE cascade,
    UNIQUE(supplier_id,product_id)
);


CREATE TABLE additional_info_attributes(
    id                     SERIAL,
    name                   VARCHAR(50) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE category_additional_info_attributes(
    category_id                      INTEGER NOT NULL,
    additional_info_attribute_id     INTEGER NOT NULL,
    FOREIGN KEY(category_id) references categories(id)
    ON DELETE CASCADE,
    FOREIGN KEY(additional_info_attribute_id) references additional_info_attributes(id)
    ON DELETE CASCADE,
    UNIQUE(category_id,additional_info_attribute_id)
);


CREATE TYPE order_and_product_status AS enum('','cart','ordered','shipped','delivered','cancelled','returned');
CREATE TABLE orders (
    id                  SERIAL,
    order_number        VARCHAR(20) UNIQUE,
    customer_id         INTEGER NOT NULL,
    address_id          INTEGER,
    cart_shipping_state CHAR(2),
    credit_card_id  INTEGER,
    amount          NUMERIC(10,2) NOT NULL CHECK(amount >= 0),
    status          order_and_product_status NOT NULL,
    tacking_id      VARCHAR(50),      
    created_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (credit_card_id) references credit_cards(id),
    FOREIGN KEY (customer_id) references customers(id),
    FOREIGN KEY (address_id) references addresses(id)
    );
CREATE INDEX orders_customer_id_key ON orders(customer_id);

CREATE TABLE order_products( 
    id              SERIAL,
    order_id        INTEGER NOT NULL,
    product_id      INTEGER NOT NULL,
    quantity        INTEGER NOT NULL CHECK(quantity > 0),
    price           NUMERIC(10,2) CHECK(price > 0.0),
    status          order_and_product_status NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (order_id) references orders(id)
    ON DELETE CASCADE,
    FOREIGN KEY (product_id) references products(id)
    ON DELETE CASCADE,
    UNIQUE(order_id,product_id)
);

CREATE TABLE stocks(
    id              SERIAL,
    quantity        INTEGER NOT NULL CHECK(quantity >= 0),    
    product_id      INTEGER NOT NULL,
    warehouse_id    INTEGER NOT NULL,
    created_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY  (id),
    FOREIGN KEY (product_id) references products(id)
    ON DELETE CASCADE,
    FOREIGN KEY(warehouse_id) references warehouses(id)
    ON DELETE CASCADE,
    UNIQUE(product_id,warehouse_id)
);

 
CREATE TYPE job_types AS enum('part_time', 'full_time');
CREATE TABLE staff_members(
    id                  SERIAL,
    name                full_name NOT NULL,
    email               VARCHAR(50) NOT NULL UNIQUE, -- Rename
    encrypted_password  VARCHAR(255) NOT NULL, -- Add
    contact_number      VARCHAR(14) NOT NULL, -- Type change
    address_id          INTEGER NOT NULL,
    job_title           VARCHAR(20) NOT NULL,
    job_type            job_types NOT NULL,
    created_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (address_id) references addresses(id)
    ON DELETE cascade      
);











