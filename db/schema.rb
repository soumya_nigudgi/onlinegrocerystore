# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "additional_info_attributes", id: :serial, force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.index ["name"], name: "additional_info_attributes_name_key", unique: true
  end

# Could not dump table "addresses" because of following StandardError
#   Unknown type 'full_name' for column 'name'

  create_table "categories", id: :serial, force: :cascade do |t|
    t.string "category_name", limit: 50, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "update_at", default: -> { "CURRENT_TIMESTAMP" }
    t.index ["category_name"], name: "categories_category_name_key", unique: true
  end

  create_table "category_additional_info_attributes", id: false, force: :cascade do |t|
    t.integer "category_id", null: false
    t.integer "additional_info_attribute_id", null: false
    t.index ["category_id", "additional_info_attribute_id"], name: "category_additional_info_attr_category_id_additional_info_a_key", unique: true
  end

  create_table "credit_cards", id: :serial, force: :cascade do |t|
    t.string "card_number", limit: 20, null: false
    t.string "name_on_card", limit: 26, null: false
    t.integer "customer_id", null: false
    t.integer "address_id", null: false
    t.date "expiry_date", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.index ["card_number"], name: "credit_cards_card_number_key", unique: true
    t.index ["customer_id"], name: "credit_cards_customer_id_key"
  end

  create_table "customer_addresses", id: false, force: :cascade do |t|
    t.integer "customer_id", null: false
    t.integer "address_id", null: false
    t.index ["customer_id", "address_id"], name: "customer_addresses_customer_id_address_id_key", unique: true
  end

# Could not dump table "customers" because of following StandardError
#   Unknown type 'full_name' for column 'name'

# Could not dump table "order_products" because of following StandardError
#   Unknown type 'order_and_product_status' for column 'status'

# Could not dump table "orders" because of following StandardError
#   Unknown type 'order_and_product_status' for column 'status'

  create_table "prices", id: :serial, force: :cascade do |t|
    t.integer "product_id", null: false
    t.decimal "price", precision: 10, scale: 2, null: false
    t.string "state", limit: 2, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.index ["product_id", "state", "price"], name: "prices_product_id_state_price_key", unique: true
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "product_name", limit: 255, null: false
    t.integer "category_id"
    t.text "product_description", null: false
    t.decimal "product_size", precision: 10, scale: 2
    t.decimal "product_weight", precision: 10, scale: 2
    t.json "additional_info"
    t.text "product_image_url"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.index ["category_id"], name: "products_category_id_key"
  end

# Could not dump table "staff_members" because of following StandardError
#   Unknown type 'full_name' for column 'name'

  create_table "stocks", id: :serial, force: :cascade do |t|
    t.integer "quantity", null: false
    t.integer "product_id", null: false
    t.integer "warehouse_id", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.index ["product_id", "warehouse_id"], name: "stocks_product_id_warehouse_id_key", unique: true
  end

  create_table "supplier_products", id: false, force: :cascade do |t|
    t.integer "supplier_id", null: false
    t.integer "product_id", null: false
    t.decimal "price", precision: 10, scale: 2, null: false
    t.index ["supplier_id", "product_id"], name: "supplier_products_supplier_id_product_id_key", unique: true
  end

# Could not dump table "suppliers" because of following StandardError
#   Unknown type 'full_name' for column 'name'

  create_table "warehouses", id: :serial, force: :cascade do |t|
    t.decimal "storage_capacity", precision: 10, scale: 2, null: false
    t.integer "address_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }
  end

  add_foreign_key "category_additional_info_attributes", "additional_info_attributes", name: "category_additional_info_attr_additional_info_attribute_id_fkey", on_delete: :cascade
  add_foreign_key "category_additional_info_attributes", "categories", name: "category_additional_info_attributes_category_id_fkey", on_delete: :cascade
  add_foreign_key "credit_cards", "addresses", name: "credit_cards_address_id_fkey", on_delete: :nullify
  add_foreign_key "credit_cards", "customers", name: "credit_cards_customer_id_fkey", on_delete: :cascade
  add_foreign_key "customer_addresses", "addresses", name: "customer_addresses_address_id_fkey", on_delete: :cascade
  add_foreign_key "customer_addresses", "customers", name: "customer_addresses_customer_id_fkey", on_delete: :cascade
  add_foreign_key "order_products", "orders", name: "order_products_order_id_fkey", on_delete: :cascade
  add_foreign_key "order_products", "products", name: "order_products_product_id_fkey", on_delete: :cascade
  add_foreign_key "orders", "addresses", name: "orders_address_id_fkey"
  add_foreign_key "orders", "credit_cards", name: "orders_credit_card_id_fkey"
  add_foreign_key "orders", "customers", name: "orders_customer_id_fkey"
  add_foreign_key "prices", "products", name: "prices_product_id_fkey", on_delete: :cascade
  add_foreign_key "products", "categories", name: "products_category_id_fkey", on_delete: :nullify
  add_foreign_key "staff_members", "addresses", name: "staff_members_address_id_fkey", on_delete: :cascade
  add_foreign_key "stocks", "products", name: "stocks_product_id_fkey", on_delete: :cascade
  add_foreign_key "stocks", "warehouses", name: "stocks_warehouse_id_fkey", on_delete: :cascade
  add_foreign_key "supplier_products", "products", name: "supplier_products_product_id_fkey", on_delete: :cascade
  add_foreign_key "supplier_products", "suppliers", name: "supplier_products_supplier_id_fkey", on_delete: :cascade
  add_foreign_key "suppliers", "addresses", name: "suppliers_address_id_fkey", on_delete: :cascade
  add_foreign_key "warehouses", "addresses", name: "warehouses_address_id_fkey", on_delete: :nullify
end
